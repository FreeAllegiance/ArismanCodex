SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       nurbs-cam_int1.1-0 ROOT ; 
       nurbs-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 21     
       handle-Back1.1-0 ; 
       handle-face1.1-0 ; 
       handle-leg_back_2.1-0 ; 
       handle-mat10_1_2.1-0 ; 
       handle-mat13_1_2.1-0 ; 
       handle-mat25.1-0 ; 
       handle-mat27.1-0 ; 
       handle-mat28.1-0 ; 
       handle-mat9_1_2.1-0 ; 
       handle-side2.1-0 ; 
       light-leg_back_1.1-0 ; 
       light-mat1_1_1.1-0 ; 
       light-mat10_1_1.1-0 ; 
       light-mat11_1_1.1-0 ; 
       light-mat13_1_1.1-0 ; 
       light-mat20_1_1.1-0 ; 
       light-mat3_1_1.1-0 ; 
       light-mat4_1_1.1-0 ; 
       light-mat9_1_1.1-0 ; 
       sample-Back.1-0 ; 
       sample-side1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 17     
       handle-bmerge1.1-0 ; 
       handle-bmerge2.1-0 ; 
       handle-bmerge3.1-0 ; 
       handle-board.1-0 ROOT ; 
       handle-cube2.1-0 ; 
       handle-cube3.1-0 ; 
       handle-cube4.1-0 ROOT ; 
       handle-model_10.1-0 ; 
       handle-model_11.1-0 ; 
       handle-model_12.1-0 ; 
       handle-model_4.1-0 ; 
       handle-model_5.1-0 ; 
       handle-model_7.1-0 ; 
       handle-model_8.1-0 ; 
       handle-model_9.1-0 ; 
       handle-null1_1.2-0 ; 
       handle-null20.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       I:/datanet/PICTURES/board ; 
       I:/datanet/PICTURES/text ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       Set-handle.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 17     
       handle-t2d11.1-0 ; 
       handle-t2d12.1-0 ; 
       handle-t2d14.1-0 ; 
       handle-t2d15.1-0 ; 
       handle-t2d24.1-0 ; 
       handle-t2d25.1-0 ; 
       handle-t2d26.1-0 ; 
       handle-t2d27.1-0 ; 
       handle-t2d28.1-0 ; 
       handle-t2d3_1_2.1-0 ; 
       handle-t2d4_1_2.1-0 ; 
       light-t2d1_1_1.1-0 ; 
       light-t2d2_1_1.1-0 ; 
       light-t2d3_1_1.1-0 ; 
       light-t2d4_1_1.1-0 ; 
       sample-t2d18.1-0 ; 
       sample-t2d19.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 1 110 ; 
       1 15 110 ; 
       2 15 110 ; 
       4 13 110 ; 
       5 7 110 ; 
       7 14 110 ; 
       8 1 110 ; 
       9 8 110 ; 
       10 1 110 ; 
       11 10 110 ; 
       12 2 110 ; 
       13 12 110 ; 
       14 2 110 ; 
       15 16 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 13 300 ; 
       0 1 300 ; 
       1 11 300 ; 
       2 17 300 ; 
       2 15 300 ; 
       3 6 300 ; 
       4 19 300 ; 
       4 20 300 ; 
       5 0 300 ; 
       5 9 300 ; 
       7 4 300 ; 
       7 7 300 ; 
       8 8 300 ; 
       9 3 300 ; 
       10 18 300 ; 
       11 12 300 ; 
       12 10 300 ; 
       13 14 300 ; 
       13 5 300 ; 
       14 2 300 ; 
       15 16 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       10 2 401 ; 
       11 11 401 ; 
       12 14 401 ; 
       13 1 401 ; 
       15 12 401 ; 
       18 13 401 ; 
       19 16 401 ; 
       20 15 401 ; 
       0 7 401 ; 
       1 0 401 ; 
       2 5 401 ; 
       3 10 401 ; 
       5 3 401 ; 
       6 4 401 ; 
       7 6 401 ; 
       8 9 401 ; 
       9 8 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 11.25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 15 0 0 DISPLAY 0 0 SRT 1 1 1 1.778838e-015 -2.008189e-016 -5.460286e-010 -9.536743e-007 -25.75036 1.043081e-007 MPRFLG 0 ; 
       4 SCHEM 10 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 12.5 -10 0 WIRECOL 9 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 12.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 7.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 7.5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 5 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 10 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 10 -8 0 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 12.5 -6 0 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 7.5 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 7.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 17.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0.7853982 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       10 SCHEM 11.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       11 SCHEM 9 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 4 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 1.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 14 -4 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       17 SCHEM 14 -6 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       18 SCHEM 6.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       19 SCHEM 9 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       20 SCHEM 9 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 11.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 1.5 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 14 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 6.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 11.5 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 14 -2 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 14 -10 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 9 -8 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 11.5 -12 0 WIRECOL 1 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       11 SCHEM 9 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 14 -8 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       13 SCHEM 6.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       14 SCHEM 4 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       15 SCHEM 9 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       16 SCHEM 9 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 1.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 1.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       2 SCHEM 11.5 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       3 SCHEM 11.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       4 SCHEM 14 -4 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       5 SCHEM 14 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       6 SCHEM 14 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       7 SCHEM 11.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       8 SCHEM 11.5 -14 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       9 SCHEM 9 -10 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 6.5 -12 0 WIRECOL 10 7 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
