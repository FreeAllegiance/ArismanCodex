SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       add_gun-aWindow.1-0 ; 
       add_gun-cam_int1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 32     
       add_gun-mat10.2-0 ; 
       add_gun-mat11.2-0 ; 
       add_gun-mat12.2-0 ; 
       add_gun-mat13.2-0 ; 
       add_gun-mat18.2-0 ; 
       add_gun-mat19.2-0 ; 
       add_gun-mat20.2-0 ; 
       add_gun-mat21.2-0 ; 
       add_gun-mat22.2-0 ; 
       add_gun-mat23.2-0 ; 
       add_gun-mat24.2-0 ; 
       add_gun-mat25.2-0 ; 
       add_gun-mat26.2-0 ; 
       add_gun-mat27.2-0 ; 
       add_gun-mat28.2-0 ; 
       add_gun-mat38.2-0 ; 
       add_gun-mat39.2-0 ; 
       add_gun-mat40.2-0 ; 
       add_gun-mat41.2-0 ; 
       edit_cargo-2.2-0 ; 
       edit_cargo-default8.2-0 ; 
       edit_cargo-mat118.2-0 ; 
       edit_cargo-mat119.2-0 ; 
       edit_cargo-mat120.2-0 ; 
       edit_cargo-mat121.2-0 ; 
       edit_cargo-mat122.2-0 ; 
       nulls-mat114.2-0 ; 
       scale-back1.2-0 ; 
       scale-front1.2-0 ; 
       scale-mat123.2-0 ; 
       scale-sides.2-0 ; 
       scale-top1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 12     
       nulls-bom05_2.2-0 ROOT ; 
       nulls-drop_tank.5-0 ; 
       nulls-finzzz0.1-0 ; 
       nulls-finzzz1.2-0 ; 
       nulls-fuselg1.1-0 ; 
       nulls-landgr1.1-0 ; 
       nulls-lfinzzz.1-0 ; 
       nulls-llandgr.1-0 ; 
       nulls-missemt.1-0 ; 
       nulls-rfinzzz.1-0 ; 
       nulls-rlandgr.1-0 ; 
       nulls1-inst.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       I:/datanet/PICTURES/utl93 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       giga_transport-giga_transport.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 26     
       add_gun-t2d10.2-0 ; 
       add_gun-t2d11.2-0 ; 
       add_gun-t2d15.2-0 ; 
       add_gun-t2d16.2-0 ; 
       add_gun-t2d17.2-0 ; 
       add_gun-t2d18.2-0 ; 
       add_gun-t2d19.2-0 ; 
       add_gun-t2d20.2-0 ; 
       add_gun-t2d21.2-0 ; 
       add_gun-t2d28.2-0 ; 
       add_gun-t2d29.2-0 ; 
       add_gun-t2d30.2-0 ; 
       add_gun-t2d9.2-0 ; 
       edit_cargo-t2d121.2-0 ; 
       edit_cargo-t2d122.2-0 ; 
       edit_cargo-t2d123.2-0 ; 
       edit_cargo-t2d125.2-0 ; 
       edit_cargo-t2d126.2-0 ; 
       edit_cargo-t2d127.2-0 ; 
       edit_cargo-t2d128.2-0 ; 
       nulls-t2d111.2-0 ; 
       scale-t2d114.2-0 ; 
       scale-t2d115.2-0 ; 
       scale-t2d116.2-0 ; 
       scale-t2d117.2-0 ; 
       scale-t2d129.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 110 ; 
       2 4 110 ; 
       3 2 110 ; 
       4 0 110 ; 
       5 4 110 ; 
       6 2 110 ; 
       7 5 110 ; 
       8 4 110 ; 
       9 2 110 ; 
       10 5 110 ; 
       11 0 20002 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       1 30 300 ; 
       1 28 300 ; 
       1 27 300 ; 
       1 31 300 ; 
       1 29 300 ; 
       3 26 300 ; 
       4 20 300 ; 
       4 21 300 ; 
       4 22 300 ; 
       4 23 300 ; 
       4 19 300 ; 
       4 24 300 ; 
       4 25 300 ; 
       5 15 300 ; 
       5 16 300 ; 
       5 17 300 ; 
       5 18 300 ; 
       6 4 300 ; 
       6 5 300 ; 
       6 6 300 ; 
       6 7 300 ; 
       7 8 300 ; 
       7 9 300 ; 
       7 10 300 ; 
       7 11 300 ; 
       9 0 300 ; 
       9 1 300 ; 
       9 2 300 ; 
       9 3 300 ; 
       10 12 300 ; 
       10 13 300 ; 
       10 14 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       0 1 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       1 12 401 ; 
       2 0 401 ; 
       3 1 401 ; 
       5 2 401 ; 
       6 3 401 ; 
       7 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       13 7 401 ; 
       14 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       18 11 401 ; 
       19 16 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       22 15 401 ; 
       23 19 401 ; 
       24 17 401 ; 
       25 18 401 ; 
       26 20 401 ; 
       27 23 401 ; 
       28 22 401 ; 
       29 25 401 ; 
       30 21 401 ; 
       31 24 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 2.5 -2 0 SRT 1 1 1 0 0 0 0 0 -0.6346477 MPRFLG 0 ; 
       1 SCHEM -5 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -6 0 MPRFLG 0 ; 
       3 SCHEM 5 -8 0 MPRFLG 0 ; 
       4 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       5 SCHEM 1.25 -6 0 MPRFLG 0 ; 
       6 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       7 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       8 SCHEM -2.5 -6 0 MPRFLG 0 ; 
       9 SCHEM 10 -8 0 MPRFLG 0 ; 
       10 SCHEM 0 -8 0 MPRFLG 0 ; 
       11 SCHEM 2.5 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 6.015357 0 -0.6346477 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 9 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 6.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 1.5 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM -1 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 4 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM 38.19267 -13.65529 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM 43.02794 -9.995279 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 34 -6 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 35.5 -14.43758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 36.6368 -14.43758 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 39.69267 -13.18033 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 41.55588 -13.23621 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM 4 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM -6 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM -6 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM -6 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM -6 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM -6 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 6.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 1.5 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -1 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 4 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 9 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 42 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 33 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 34.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 37.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 39 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 40.5 -8 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 36.6368 -16.43758 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 4 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM -6 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM -6 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM -6 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM -6 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM -6 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 109 109 1 
       PAUSE 0 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
