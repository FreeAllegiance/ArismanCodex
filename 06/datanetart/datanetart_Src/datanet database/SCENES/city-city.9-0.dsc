SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       city-cam_int1.9-0 ROOT ; 
       city-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       city-inf_light1.8-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 43     
       city-default1.2-0 ; 
       city-mat1.2-0 ; 
       city-mat10.2-0 ; 
       city-mat11.2-0 ; 
       city-mat12.2-0 ; 
       city-mat13.2-0 ; 
       city-mat14.2-0 ; 
       city-mat15.2-0 ; 
       city-mat16.2-0 ; 
       city-mat17.2-0 ; 
       city-mat18.2-0 ; 
       city-mat19.2-0 ; 
       city-mat2.2-0 ; 
       city-mat20.2-0 ; 
       city-mat21.2-0 ; 
       city-mat22.2-0 ; 
       city-mat23.2-0 ; 
       city-mat24.2-0 ; 
       city-mat25.2-0 ; 
       city-mat26.2-0 ; 
       city-mat27.2-0 ; 
       city-mat28.2-0 ; 
       city-mat29.2-0 ; 
       city-mat3.2-0 ; 
       city-mat30.2-0 ; 
       city-mat31.2-0 ; 
       city-mat32.2-0 ; 
       city-mat33.2-0 ; 
       city-mat34.2-0 ; 
       city-mat35.2-0 ; 
       city-mat36.2-0 ; 
       city-mat37.2-0 ; 
       city-mat38.2-0 ; 
       city-mat39.2-0 ; 
       city-mat4.2-0 ; 
       city-mat40.2-0 ; 
       city-mat41.2-0 ; 
       city-mat42.2-0 ; 
       city-mat5.2-0 ; 
       city-mat6.2-0 ; 
       city-mat7.2-0 ; 
       city-mat8.2-0 ; 
       city-mat9.2-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 22     
       city-cube1.6-0 ROOT ; 
       city-inst1.2-0 ROOT ; 
       city-inst2.2-0 ROOT ; 
       Ground-Ground.5-0 ROOT ; 
       utl20-ahubcon.1-0 ; 
       utl20-armour.1-0 ; 
       utl20-doccon.1-0 ; 
       utl20-engine.1-0 ; 
       utl20-fhubcon.1-0 ; 
       utl20-fuselg1.5-0 ROOT ; 
       utl20-fuselg2.1-0 ; 
       utl20-ldoccon.1-0 ; 
       utl20-lights1.1-0 ; 
       utl20-lights2.1-0 ; 
       utl20-rdoccon.1-0 ; 
       utl20-tractr1.1-0 ; 
       utl20-tractr2.1-0 ; 
       utl20-tractr3.1-0 ; 
       utl21-inst.2-0 ROOT ; 
       utl22-inst.2-0 ROOT ; 
       utl23-inst.2-0 ROOT ; 
       Vol-Vol.3-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 1     
       I:/datanet/PICTURES/utl20 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       city-city.9-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 33     
       city-t2d1.1-0 ; 
       city-t2d10.1-0 ; 
       city-t2d11.1-0 ; 
       city-t2d12.1-0 ; 
       city-t2d13.1-0 ; 
       city-t2d14.1-0 ; 
       city-t2d15.1-0 ; 
       city-t2d16.1-0 ; 
       city-t2d17.1-0 ; 
       city-t2d18.1-0 ; 
       city-t2d19.1-0 ; 
       city-t2d2.1-0 ; 
       city-t2d20.1-0 ; 
       city-t2d21.1-0 ; 
       city-t2d22.1-0 ; 
       city-t2d23.1-0 ; 
       city-t2d24.1-0 ; 
       city-t2d25.1-0 ; 
       city-t2d26.1-0 ; 
       city-t2d27.1-0 ; 
       city-t2d28.1-0 ; 
       city-t2d29.1-0 ; 
       city-t2d3.1-0 ; 
       city-t2d30.1-0 ; 
       city-t2d31.1-0 ; 
       city-t2d32.1-0 ; 
       city-t2d33.1-0 ; 
       city-t2d4.1-0 ; 
       city-t2d5.1-0 ; 
       city-t2d6.1-0 ; 
       city-t2d7.1-0 ; 
       city-t2d8.1-0 ; 
       city-t2d9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       city-vol1.3-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       1 0 20002 ; 
       2 0 20002 ; 
       4 9 110 ; 
       5 9 110 ; 
       6 5 110 ; 
       7 10 110 ; 
       8 9 110 ; 
       10 9 110 ; 
       11 8 110 ; 
       12 9 110 ; 
       13 12 110 ; 
       14 8 110 ; 
       15 9 110 ; 
       16 15 110 ; 
       17 16 110 ; 
       18 9 20002 ; 
       19 9 20002 ; 
       20 9 20002 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       3 12 300 ; 
       4 42 300 ; 
       4 2 300 ; 
       4 3 300 ; 
       4 4 300 ; 
       5 30 300 ; 
       5 31 300 ; 
       5 32 300 ; 
       5 33 300 ; 
       5 35 300 ; 
       5 36 300 ; 
       5 37 300 ; 
       7 11 300 ; 
       8 18 300 ; 
       8 19 300 ; 
       8 20 300 ; 
       9 0 300 ; 
       9 23 300 ; 
       9 34 300 ; 
       9 38 300 ; 
       9 39 300 ; 
       9 40 300 ; 
       9 41 300 ; 
       10 5 300 ; 
       10 6 300 ; 
       10 7 300 ; 
       10 8 300 ; 
       10 9 300 ; 
       10 10 300 ; 
       12 13 300 ; 
       12 14 300 ; 
       12 15 300 ; 
       12 16 300 ; 
       13 17 300 ; 
       15 21 300 ; 
       16 22 300 ; 
       16 24 300 ; 
       16 25 300 ; 
       17 26 300 ; 
       17 27 300 ; 
       17 28 300 ; 
       17 29 300 ; 
       21 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       2 30 401 ; 
       3 31 401 ; 
       4 32 401 ; 
       6 1 401 ; 
       7 2 401 ; 
       8 3 401 ; 
       9 4 401 ; 
       10 5 401 ; 
       11 6 401 ; 
       14 7 401 ; 
       15 8 401 ; 
       16 9 401 ; 
       17 10 401 ; 
       19 12 401 ; 
       20 13 401 ; 
       21 14 401 ; 
       23 0 401 ; 
       24 15 401 ; 
       25 16 401 ; 
       27 17 401 ; 
       28 18 401 ; 
       29 19 401 ; 
       31 20 401 ; 
       32 21 401 ; 
       33 23 401 ; 
       34 11 401 ; 
       35 24 401 ; 
       36 25 401 ; 
       37 26 401 ; 
       38 22 401 ; 
       39 27 401 ; 
       40 28 401 ; 
       41 29 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       1 0 550 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 0 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 20 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 10 -2 0 SRT 0.472 4.206691 0.472 0 0 0 59.85548 14.9904 -55.80426 MPRFLG 0 ; 
       1 SCHEM 10 0 0 WIRECOL 5 7 SRT 0.472 4.206691 0.472 0 0 0 39.37038 14.9904 -48.74043 MPRFLG 0 ; 
       2 SCHEM 12.5 0 0 WIRECOL 5 7 SRT 0.472 6.6634 0.472 0 0 0 76.10229 14.9904 -44.50213 MPRFLG 0 ; 
       3 SCHEM 15 0 0 SRT 9.349998 9.349998 9.349998 0 0 0 0 -6.623118 0 MPRFLG 1 ; 
       4 SCHEM 35 -4 0 MPRFLG 0 ; 
       5 SCHEM 20 -4 0 MPRFLG 0 ; 
       6 SCHEM 11.25 -6 0 MPRFLG 0 ; 
       7 SCHEM -51.25 -6 0 MPRFLG 0 ; 
       8 SCHEM -16.25 -4 0 MPRFLG 0 ; 
       9 SCHEM 2.5 -2 0 SRT 0.7763401 0.7763401 0.7763401 1.570796 -3.821371e-015 3.141593 -75.33823 3.294116 46.59184 MPRFLG 0 ; 
       10 SCHEM -43.75 -4 0 MPRFLG 0 ; 
       11 SCHEM -18.75 -6 0 MPRFLG 0 ; 
       12 SCHEM -28.75 -4 0 MPRFLG 0 ; 
       13 SCHEM -33.75 -6 0 MPRFLG 0 ; 
       14 SCHEM -21.25 -6 0 MPRFLG 0 ; 
       15 SCHEM 0 -4 0 MPRFLG 0 ; 
       16 SCHEM -1.25 -6 0 MPRFLG 0 ; 
       17 SCHEM -5 -8 0 MPRFLG 0 ; 
       18 SCHEM 2.5 0 0 WIRECOL 5 7 SRT 0.7763401 0.7763401 0.7763401 1.570796 -3.821371e-015 3.141593 46.58343 4.154026 -20.14556 MPRFLG 0 ; 
       19 SCHEM 5 0 0 WIRECOL 5 7 SRT 0.7763401 0.7763401 0.7763401 1.570796 -3.821371e-015 3.141593 53.64725 6.948846 11.64168 MPRFLG 0 ; 
       20 SCHEM 7.5 0 0 WIRECOL 5 7 SRT 0.7763401 0.7763401 0.7763401 1.570796 -3.821371e-015 3.141593 62.83024 10.50589 32.83317 MPRFLG 0 ; 
       21 SCHEM 17.5 0 0 SRT 25.54837 25.54837 25.54837 0 -0.7300003 0 4.227172 0 -7.850462 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 56.25 -4 0 WIRECOL 1 7 DISPLAY 1 2 MPRFLG 0 ; 
       1 SCHEM 17.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 31.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 33.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 36.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM -36.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM -48.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM -46.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM -43.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM -41.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM -38.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM -51.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM -23.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM -31.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM -28.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM -26.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM -33.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM -11.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       19 SCHEM -16.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       20 SCHEM -13.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       21 SCHEM 8.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       22 SCHEM 6.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       23 SCHEM 41.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       24 SCHEM 1.25 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       25 SCHEM 3.75 -8 0 WIRECOL 1 7 MPRFLG 0 ; 
       26 SCHEM -1.25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       27 SCHEM -8.75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       28 SCHEM -6.25 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       29 SCHEM -3.75 -10 0 WIRECOL 1 7 MPRFLG 0 ; 
       30 SCHEM 28.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       31 SCHEM 13.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       32 SCHEM 16.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       33 SCHEM 18.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       34 SCHEM 43.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       35 SCHEM 21.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       36 SCHEM 23.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       37 SCHEM 26.25 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       38 SCHEM 46.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       39 SCHEM 48.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       40 SCHEM 51.25 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       41 SCHEM 53.75 -4 0 WIRECOL 1 7 MPRFLG 0 ; 
       42 SCHEM 38.75 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 41.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM -48.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM -46.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM -43.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM -41.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM -38.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM -51.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM -31.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM -28.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM -26.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM -33.75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 43.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM -16.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM -13.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 8.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 1.25 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 3.75 -10 0 WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM -8.75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM -6.25 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM -3.75 -12 0 WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 13.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 16.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 46.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 18.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 21.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 23.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 26.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 48.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 51.25 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 53.75 -6 0 WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 31.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 33.75 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 36.25 -8 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
