SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       debris-cam_int1.1-0 ROOT ; 
       debris-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       debris-mat1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 5     
       debris-Nebula1.1-0 ROOT ; 
       debris-Nebula2.1-0 ROOT ; 
       debris-Nebula3.1-0 ROOT ; 
       debris-Nebula4.1-0 ROOT ; 
       Round-round-neb.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       36-debris.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       1 0 300 ; 
       4 0 300 ; 
       2 0 300 ; 
       3 0 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       1 SCHEM 7.5 0 0 SRT 1 1 1 0 0 0 -7.57149 0 -1.325011 MPRFLG 0 ; 
       4 SCHEM 2.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 10 0 0 SRT 0.5239999 0.5239999 0.5239999 0 0 0 -1.703585 0 -1.325011 MPRFLG 0 ; 
       3 SCHEM 12.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 2.5 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 2 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
