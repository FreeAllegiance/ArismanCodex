SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       set_up-cam_int1.1-0 ROOT ; 
       set_up-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 2     
       Lighting_Template-inf_light1_1.1-0 ROOT ; 
       Lighting_Template-inf_light2_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 1     
       asteroid-default1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 2     
       asteroid-dodeca1.1-0 ROOT ; 
       asteroid-sphere2.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 8     
       I:/datanet/PICTURES/MoonBump ; 
       I:/datanet/PICTURES/MoonMap ; 
       I:/datanet/PICTURES/bottom_of_features ; 
       I:/datanet/PICTURES/inside_large_crater ; 
       //research/root/federation/Shared_Art_Files/SoftImage_Archive/Softimage_Disk3/Space_Ambiance/meteors/bgrnd05/PICTURES/large crater ; 
       I:/datanet/PICTURES/main_feature ; 
       I:/datanet/PICTURES/main_shell ; 
       I:/datanet/PICTURES/map1 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ruins-asteroid.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 8     
       asteroid-FeatureBumps2.1-0 ; 
       asteroid-FeatureMap2.1-0 ; 
       asteroid-Inside_large_Crater2.1-0 ; 
       asteroid-Large_Crater2.1-0 ; 
       asteroid-MoonBump1.1-0 ; 
       asteroid-MoonMap1.1-0 ; 
       asteroid-Shell2.1-0 ; 
       asteroid-t2d4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       asteroid-rock2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MATERIALS 
       1 0 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       1 4 400 ; 
       1 5 400 ; 
       1 6 400 ; 
       1 3 400 ; 
       1 2 400 ; 
       1 0 400 ; 
       1 1 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       1 0 500 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       0 7 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 25 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 27.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       1 SCHEM 12.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 30 0 0 DISPLAY 1 2 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 15 -2 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 20 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 17.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 12.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 2.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 7.5 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 15 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 10 -2 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
