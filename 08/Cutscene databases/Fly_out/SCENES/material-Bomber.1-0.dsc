SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       helmet-cam_int1.2-0 ROOT ; 
       helmet-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 4     
       vol-bom_vol.1-0 ROOT ; 
       vol-bom_vol1.1-0 ROOT ; 
       vol-bom_vol2.1-0 ROOT ; 
       vol-bom_vol3.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 4     
       Bomber-bom_vol_1.1-0 ; 
       Bomber-bom_vol_2.1-0 ; 
       Bomber-bom_vol_3.1-0 ; 
       Bomber-bom_vol_4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 19     
       Bomber-Arm1.1-0 ; 
       Bomber-back_blocker.1-0 ; 
       Bomber-Black1.1-0 ; 
       Bomber-bom_Vol1.1-0 ; 
       Bomber-Canopy.1-0 ; 
       Bomber-copper1.1-0 ; 
       Bomber-Green1.1-0 ; 
       Bomber-gun1.1-0 ; 
       Bomber-mat423.1-0 ; 
       Bomber-mat428.1-0 ; 
       Bomber-mat432.1-0 ; 
       Bomber-mat436.1-0 ; 
       Bomber-mat442.1-0 ; 
       Bomber-mat443.1-0 ; 
       Bomber-METALS-chrome001.1-0.1-0 ; 
       Bomber-METALS-GRAPHITE.1-2.1-0 ; 
       Bomber-METALS-GRAPHITE.1-3.1-0 ; 
       strobes-mat448.1-0 ; 
       strobes-mat449.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 128     
       Bomber-BIG_CANOPY.1-0 ; 
       Bomber-bom_vol.1-0 ; 
       Bomber-bottom20.1-0 ; 
       Bomber-cube41.3-0 ; 
       Bomber-cube42.1-0 ; 
       Bomber-cube44.1-0 ; 
       Bomber-cube45.1-0 ; 
       Bomber-cube47.1-0 ; 
       Bomber-cube48.1-0 ; 
       Bomber-cube49.1-0 ; 
       Bomber-cube50.1-0 ; 
       Bomber-cube51.1-0 ; 
       Bomber-cube52.1-0 ; 
       Bomber-cube53.1-0 ; 
       Bomber-extru2.1-0 ; 
       Bomber-extru42.1-0 ; 
       Bomber-extru53.1-0 ; 
       Bomber-extru57.1-0 ; 
       Bomber-extru58.1-0 ; 
       Bomber-extru60.1-0 ; 
       Bomber-extru7.1-0 ; 
       Bomber-face1.1-0 ; 
       Bomber-face2.1-0 ; 
       Bomber-FUSELAGE.1-0 ; 
       Bomber-light_holder_1.1-0 ; 
       Bomber-light_holder_2.1-0 ; 
       Bomber-light_holder_3.1-0 ; 
       Bomber-light_holder_4.1-0 ; 
       Bomber-lwingzz1.1-0 ; 
       Bomber-null11.1-0 ; 
       Bomber-null12.1-0 ; 
       Bomber-null2.1-0 ; 
       Bomber-null3.1-0 ; 
       Bomber-null7.1-0 ; 
       Bomber-null8.1-0 ; 
       Bomber-nurbs22.1-0 ; 
       Bomber-nurbs25.1-0 ; 
       Bomber-nurbs27.1-0 ; 
       Bomber-nurbs33.1-0 ; 
       Bomber-nurbs37.1-0 ; 
       Bomber-nurbs38.1-0 ; 
       Bomber-nurbs8.1-0 ; 
       Bomber-revol100.1-0 ; 
       Bomber-revol102.1-0 ; 
       Bomber-revol107.1-0 ; 
       Bomber-revol108.1-0 ; 
       Bomber-revol110.1-0 ; 
       Bomber-revol111.1-0 ; 
       Bomber-revol112.1-0 ; 
       Bomber-revol113.1-0 ; 
       Bomber-revol114.1-0 ; 
       Bomber-revol115.1-0 ; 
       Bomber-revol116.1-0 ; 
       Bomber-revol117.1-0 ; 
       Bomber-revol118.1-0 ; 
       Bomber-revol119.1-0 ; 
       Bomber-revol120.1-0 ; 
       Bomber-revol121.1-0 ; 
       Bomber-revol122.1-0 ; 
       Bomber-revol123.1-0 ; 
       Bomber-revol124.1-0 ; 
       Bomber-revol125.1-0 ; 
       Bomber-revol126.1-0 ; 
       Bomber-revol127.1-0 ; 
       Bomber-revol128.1-0 ; 
       Bomber-revol129.1-0 ; 
       Bomber-revol130.1-0 ; 
       Bomber-revol131.1-0 ; 
       Bomber-revol132.1-0 ; 
       Bomber-revol133.1-0 ; 
       Bomber-revol134.1-0 ; 
       Bomber-revol135.1-0 ; 
       Bomber-revol136.1-0 ; 
       Bomber-revol137.1-0 ; 
       Bomber-revol138.1-0 ; 
       Bomber-revol139.1-0 ; 
       Bomber-revol140.1-0 ; 
       Bomber-revol141.1-0 ; 
       Bomber-revol142.1-0 ; 
       Bomber-revol143.1-0 ; 
       Bomber-revol144.1-0 ; 
       Bomber-revol145.1-0 ; 
       Bomber-revol146.1-0 ; 
       Bomber-revol147.1-0 ; 
       Bomber-revol57.1-0 ; 
       Bomber-revol58.1-0 ; 
       Bomber-revol59.1-0 ; 
       Bomber-revol60.1-0 ; 
       Bomber-revol65.1-0 ; 
       Bomber-revol68.1-0 ; 
       Bomber-revol82.1-0 ; 
       Bomber-revol83.1-0 ; 
       Bomber-revol84.1-0 ; 
       Bomber-revol85.1-0 ; 
       Bomber-revol86.1-0 ; 
       Bomber-revol87.1-0 ; 
       Bomber-revol88.1-0 ; 
       Bomber-revol89.1-0 ; 
       Bomber-revol90.1-0 ; 
       Bomber-revol91.1-0 ; 
       Bomber-revol92.1-0 ; 
       Bomber-revol94.1-0 ; 
       Bomber-revol99.1-0 ; 
       Bomber-Root.1-0 ROOT ; 
       Bomber-rwingzz1.1-0 ; 
       Bomber-skin10.1-0 ; 
       Bomber-skin11.1-0 ; 
       Bomber-skin12.1-0 ; 
       Bomber-skin13.1-0 ; 
       Bomber-skin14.1-0 ; 
       Bomber-skin15.1-0 ; 
       Bomber-skin16.1-0 ; 
       Bomber-skin17.1-0 ; 
       Bomber-skin19.1-0 ; 
       Bomber-skin20.1-0 ; 
       Bomber-skin23.1-0 ; 
       Bomber-skin24.1-0 ; 
       Bomber-skin26.2-0 ; 
       Bomber-skin4.1-0 ; 
       Bomber-skin5.1-0 ; 
       Bomber-skin6.1-0 ; 
       Bomber-skin7.1-0 ; 
       Bomber-strobe_GN_1.1-0 ; 
       Bomber-strobe_GN_2.1-0 ; 
       Bomber-strobe_GN_3.1-0 ; 
       Bomber-strobe_RD_1.1-0 ; 
       Bomber-strobe_RD_2.1-0 ; 
       Bomber-strobe_RD_3.1-0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       strobes-DGlow1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 31     
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Bars ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Eat_to_the_Beat ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Text ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Text-flip ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/arrow ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/backgr ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/black ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/civ_logo ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/cube ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/cubes ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fin_bump ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fin_bump_lines ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fin_spec_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fin_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fuge_bump ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fuge_bump_lines ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fuge_spec_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fuge_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fuselage ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/hazzard ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/panel ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/soot ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/specular_grain ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/tail_bump ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/tail_bump_lines ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/tail_spec_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/tail_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/thrust_bump ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/thrust_bump_lines ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/thrust_spec_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/thrust_tiles ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       material-Bomber.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 80     
       Bomber-arm_Bump.1-0 ; 
       Bomber-arm_Bump1.1-0 ; 
       Bomber-Black1.1-0 ; 
       Bomber-Black2.1-0 ; 
       Bomber-Bump_Lines1.1-0 ; 
       Bomber-Bump_Lines2.1-0 ; 
       Bomber-Bump_Lines3.1-0 ; 
       Bomber-Bump_Lines4.1-0 ; 
       Bomber-Bump_Lines5.1-0 ; 
       Bomber-Bump_Lines6.1-0 ; 
       Bomber-Bump_Lines7.1-0 ; 
       Bomber-Bump_Lines8.1-0 ; 
       Bomber-Bump_Lines9.1-0 ; 
       Bomber-Cubes1.1-0 ; 
       Bomber-Cubes2.1-0 ; 
       Bomber-Cubes3.1-0 ; 
       Bomber-Cubes4.1-0 ; 
       Bomber-Cubes5.1-0 ; 
       Bomber-Cubes6.1-0 ; 
       Bomber-Cubes7.1-0 ; 
       Bomber-Cubes8.1-0 ; 
       Bomber-Cubes9.1-0 ; 
       Bomber-Fin_Bump.1-0 ; 
       Bomber-Fuge_Bump1.1-0 ; 
       Bomber-Fuge_Bump3.1-0 ; 
       Bomber-Fuge_Bump4.1-0 ; 
       Bomber-Hazzard1.1-0 ; 
       Bomber-Hazzard2.1-0 ; 
       Bomber-soot1.1-0 ; 
       Bomber-Specular_Grain1.1-0 ; 
       Bomber-Specular_Grain2.1-0 ; 
       Bomber-Specular_Grain3.1-0 ; 
       Bomber-Specular_Grain4.1-0 ; 
       Bomber-Specular_Grain5.1-0 ; 
       Bomber-Specular_Grain6.1-0 ; 
       Bomber-Specular_Grain7.1-0 ; 
       Bomber-Specular_Grain8.1-0 ; 
       Bomber-Specular_Grain9.1-0 ; 
       Bomber-Spec_Killer1.1-0 ; 
       Bomber-Spec_Killer2.1-0 ; 
       Bomber-Spec_Killer3.1-0 ; 
       Bomber-Spec_Killer4.1-0 ; 
       Bomber-Spec_Killer5.1-0 ; 
       Bomber-Spec_Killer6.1-0 ; 
       Bomber-Spec_Killer7.1-0 ; 
       Bomber-Spec_Killer8.1-0 ; 
       Bomber-Spec_Killer9.1-0 ; 
       Bomber-Spec_tiles1.1-0 ; 
       Bomber-Spec_tiles2.1-0 ; 
       Bomber-Spec_tiles3.1-0 ; 
       Bomber-Spec_tiles4.1-0 ; 
       Bomber-Spec_tiles5.1-0 ; 
       Bomber-Spec_tiles6.1-0 ; 
       Bomber-Spec_tiles7.1-0 ; 
       Bomber-Spec_tiles8.1-0 ; 
       Bomber-Spec_tiles9.1-0 ; 
       Bomber-t2d200.1-0 ; 
       Bomber-t2d201.1-0 ; 
       Bomber-t2d202.1-0 ; 
       Bomber-t2d203.1-0 ; 
       Bomber-t2d204.1-0 ; 
       Bomber-t2d205.1-0 ; 
       Bomber-t2d206.1-0 ; 
       Bomber-t2d207.1-0 ; 
       Bomber-t2d208.1-0 ; 
       Bomber-t2d209.1-0 ; 
       Bomber-t2d210.1-0 ; 
       Bomber-t2d211.1-0 ; 
       Bomber-tail_Bump.1-0 ; 
       Bomber-Thrust_Bump.1-0 ; 
       Bomber-Thrust_Bump1.1-0 ; 
       Bomber-Tiles1.1-0 ; 
       Bomber-Tiles2.1-0 ; 
       Bomber-Tiles3.1-0 ; 
       Bomber-Tiles4.1-0 ; 
       Bomber-Tiles5.1-0 ; 
       Bomber-Tiles6.1-0 ; 
       Bomber-Tiles7.1-0 ; 
       Bomber-Tiles8.1-0 ; 
       Bomber-Tiles9.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       Bomber-bom_Vol_1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       1 23 110 ; 
       2 33 110 ; 
       3 8 110 ; 
       4 113 110 ; 
       5 116 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 14 110 ; 
       9 14 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 121 110 ; 
       14 23 110 ; 
       15 14 110 ; 
       16 10 110 ; 
       17 33 110 ; 
       18 23 110 ; 
       19 121 110 ; 
       20 3 110 ; 
       21 14 110 ; 
       22 15 110 ; 
       23 103 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 15 110 ; 
       29 23 110 ; 
       30 103 110 ; 
       31 23 110 ; 
       32 23 110 ; 
       33 23 110 ; 
       34 23 110 ; 
       35 116 110 ; 
       36 102 110 ; 
       37 102 110 ; 
       38 0 110 ; 
       39 43 110 ; 
       40 43 110 ; 
       41 113 110 ; 
       42 101 110 ; 
       43 29 110 ; 
       44 121 110 ; 
       45 44 110 ; 
       46 117 110 ; 
       47 117 110 ; 
       48 100 110 ; 
       49 99 110 ; 
       50 98 110 ; 
       51 97 110 ; 
       52 90 110 ; 
       53 91 110 ; 
       54 92 110 ; 
       55 93 110 ; 
       56 94 110 ; 
       57 95 110 ; 
       58 96 110 ; 
       59 113 110 ; 
       60 59 110 ; 
       61 113 110 ; 
       62 61 110 ; 
       63 113 110 ; 
       64 63 110 ; 
       65 113 110 ; 
       66 65 110 ; 
       67 113 110 ; 
       68 67 110 ; 
       69 113 110 ; 
       70 69 110 ; 
       71 113 110 ; 
       72 71 110 ; 
       73 113 110 ; 
       74 73 110 ; 
       75 113 110 ; 
       76 75 110 ; 
       77 113 110 ; 
       78 77 110 ; 
       79 113 110 ; 
       80 79 110 ; 
       81 121 110 ; 
       82 81 110 ; 
       83 42 110 ; 
       84 15 110 ; 
       85 15 110 ; 
       86 28 110 ; 
       87 15 110 ; 
       88 89 110 ; 
       89 101 110 ; 
       90 116 110 ; 
       91 116 110 ; 
       92 116 110 ; 
       93 116 110 ; 
       94 116 110 ; 
       95 116 110 ; 
       96 116 110 ; 
       97 116 110 ; 
       98 116 110 ; 
       99 116 110 ; 
       100 116 110 ; 
       101 23 110 ; 
       102 34 110 ; 
       104 15 110 ; 
       105 31 110 ; 
       106 31 110 ; 
       107 31 110 ; 
       108 31 110 ; 
       109 32 110 ; 
       110 32 110 ; 
       111 32 110 ; 
       112 32 110 ; 
       113 114 110 ; 
       114 23 110 ; 
       115 23 110 ; 
       116 115 110 ; 
       117 14 110 ; 
       118 121 110 ; 
       119 121 110 ; 
       120 121 110 ; 
       121 23 110 ; 
       122 30 110 ; 
       123 30 110 ; 
       124 30 110 ; 
       125 30 110 ; 
       126 30 110 ; 
       127 30 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 4 300 ; 
       1 3 300 ; 
       3 6 300 ; 
       4 14 300 ; 
       5 14 300 ; 
       6 14 300 ; 
       7 14 300 ; 
       8 6 300 ; 
       9 6 300 ; 
       10 6 300 ; 
       11 14 300 ; 
       12 14 300 ; 
       13 14 300 ; 
       14 6 300 ; 
       15 6 300 ; 
       16 6 300 ; 
       17 11 300 ; 
       18 2 300 ; 
       20 6 300 ; 
       21 1 300 ; 
       23 6 300 ; 
       28 15 300 ; 
       35 16 300 ; 
       36 2 300 ; 
       37 14 300 ; 
       38 6 300 ; 
       39 14 300 ; 
       40 2 300 ; 
       41 16 300 ; 
       42 6 300 ; 
       43 6 300 ; 
       44 14 300 ; 
       45 15 300 ; 
       46 7 300 ; 
       47 7 300 ; 
       48 13 300 ; 
       49 13 300 ; 
       50 13 300 ; 
       51 13 300 ; 
       52 13 300 ; 
       53 13 300 ; 
       54 13 300 ; 
       55 13 300 ; 
       56 13 300 ; 
       57 13 300 ; 
       58 13 300 ; 
       59 15 300 ; 
       60 13 300 ; 
       61 15 300 ; 
       62 13 300 ; 
       63 15 300 ; 
       64 13 300 ; 
       65 15 300 ; 
       66 13 300 ; 
       67 15 300 ; 
       68 13 300 ; 
       69 15 300 ; 
       70 13 300 ; 
       71 15 300 ; 
       72 13 300 ; 
       73 15 300 ; 
       74 13 300 ; 
       75 15 300 ; 
       76 13 300 ; 
       77 15 300 ; 
       78 13 300 ; 
       79 15 300 ; 
       80 13 300 ; 
       81 14 300 ; 
       82 15 300 ; 
       83 12 300 ; 
       84 0 300 ; 
       85 15 300 ; 
       86 16 300 ; 
       87 15 300 ; 
       88 12 300 ; 
       89 6 300 ; 
       90 15 300 ; 
       91 15 300 ; 
       92 15 300 ; 
       93 15 300 ; 
       94 15 300 ; 
       95 15 300 ; 
       96 15 300 ; 
       97 15 300 ; 
       98 15 300 ; 
       99 15 300 ; 
       100 15 300 ; 
       102 6 300 ; 
       104 15 300 ; 
       105 14 300 ; 
       106 14 300 ; 
       107 14 300 ; 
       108 14 300 ; 
       109 14 300 ; 
       110 14 300 ; 
       111 14 300 ; 
       112 14 300 ; 
       113 6 300 ; 
       114 0 300 ; 
       115 0 300 ; 
       116 6 300 ; 
       117 10 300 ; 
       118 14 300 ; 
       119 14 300 ; 
       120 14 300 ; 
       121 9 300 ; 
       122 17 300 ; 
       123 17 300 ; 
       124 17 300 ; 
       125 18 300 ; 
       126 18 300 ; 
       127 18 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 58 400 ; 
       14 68 400 ; 
       14 39 400 ; 
       14 72 400 ; 
       14 5 400 ; 
       14 48 400 ; 
       14 14 400 ; 
       14 30 400 ; 
       14 62 400 ; 
       14 63 400 ; 
       15 25 400 ; 
       15 46 400 ; 
       15 79 400 ; 
       15 12 400 ; 
       15 55 400 ; 
       15 21 400 ; 
       15 37 400 ; 
       15 3 400 ; 
       15 66 400 ; 
       16 24 400 ; 
       16 41 400 ; 
       16 74 400 ; 
       16 7 400 ; 
       16 50 400 ; 
       16 16 400 ; 
       16 32 400 ; 
       20 22 400 ; 
       20 40 400 ; 
       20 73 400 ; 
       20 6 400 ; 
       20 49 400 ; 
       20 15 400 ; 
       20 31 400 ; 
       20 67 400 ; 
       21 65 400 ; 
       23 23 400 ; 
       23 38 400 ; 
       23 71 400 ; 
       23 4 400 ; 
       23 47 400 ; 
       23 13 400 ; 
       23 29 400 ; 
       23 2 400 ; 
       23 28 400 ; 
       23 59 400 ; 
       23 64 400 ; 
       38 57 400 ; 
       43 70 400 ; 
       43 43 400 ; 
       43 76 400 ; 
       43 9 400 ; 
       43 52 400 ; 
       43 18 400 ; 
       43 34 400 ; 
       43 27 400 ; 
       102 69 400 ; 
       102 42 400 ; 
       102 75 400 ; 
       102 8 400 ; 
       102 51 400 ; 
       102 17 400 ; 
       102 33 400 ; 
       102 26 400 ; 
       113 0 400 ; 
       113 44 400 ; 
       113 77 400 ; 
       113 10 400 ; 
       113 53 400 ; 
       113 19 400 ; 
       113 35 400 ; 
       113 60 400 ; 
       116 1 400 ; 
       116 45 400 ; 
       116 78 400 ; 
       116 11 400 ; 
       116 54 400 ; 
       116 20 400 ; 
       116 36 400 ; 
       116 61 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER MODELS 
       0 24 2111 ; 
       1 25 2111 ; 
       2 26 2111 ; 
       3 27 2111 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       16 56 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       3 0 550 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       0 0 550 ; 
       1 1 550 ; 
       2 2 550 ; 
       3 3 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 122 551 1 ; 
       0 125 551 1 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 0 551 1 ; 
       0 1 551 1 ; 
       0 2 551 1 ; 
       0 3 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 84.06493 -26.01294 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 86.54095 -26.28573 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 89.04095 -26.28573 0 USR WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 91.54095 -26.28573 0 USR WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 37.6747 -1.05336 0 MPRFLG 0 ; 
       1 SCHEM 55.1747 0.9466555 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 35.17469 -1.05336 0 MPRFLG 0 ; 
       3 SCHEM -1.20716 -18.82974 0 MPRFLG 0 ; 
       4 SCHEM 80.16441 -30.01663 0 MPRFLG 0 ; 
       5 SCHEM 60.77762 -21.41134 0 MPRFLG 0 ; 
       6 SCHEM -1.20716 -20.82974 0 MPRFLG 0 ; 
       7 SCHEM 1.292846 -20.82974 0 MPRFLG 0 ; 
       8 SCHEM -1.20716 -16.82972 0 MPRFLG 0 ; 
       9 SCHEM 6.292845 -16.82972 0 MPRFLG 0 ; 
       10 SCHEM 6.292845 -18.82974 0 MPRFLG 0 ; 
       11 SCHEM 6.292845 -20.82974 0 MPRFLG 0 ; 
       12 SCHEM 8.792845 -20.82974 0 MPRFLG 0 ; 
       13 SCHEM 30.17469 -1.05336 0 MPRFLG 0 ; 
       14 SCHEM 6.292845 -14.82973 0 USR MPRFLG 0 ; 
       15 SCHEM 17.54283 -16.82972 0 MPRFLG 0 ; 
       16 SCHEM 3.792845 -20.82974 0 MPRFLG 0 ; 
       17 SCHEM 32.67469 -1.05336 0 MPRFLG 0 ; 
       18 SCHEM 37.6747 0.9466555 0 MPRFLG 0 ; 
       19 SCHEM 27.67468 -1.05336 0 MPRFLG 0 ; 
       20 SCHEM -3.707167 -20.82974 0 MPRFLG 0 ; 
       21 SCHEM -6.207152 -16.82972 0 MPRFLG 0 ; 
       22 SCHEM 21.29283 -18.82974 0 MPRFLG 0 ; 
       23 SCHEM 36.42469 2.946656 0 MPRFLG 0 ; 
       24 SCHEM 80.75351 -17.69821 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 83.25351 -17.69821 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 85.75352 -17.69821 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 88.25352 -17.69821 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 13.79284 -18.82974 0 MPRFLG 0 ; 
       29 SCHEM 72.00351 -17.69821 0 USR MPRFLG 0 ; 
       30 SCHEM 29.58685 -7.261245 0 USR MPRFLG 0 ; 
       31 SCHEM 43.92469 0.9466555 0 MPRFLG 0 ; 
       32 SCHEM 61.42473 0.9466555 0 MPRFLG 0 ; 
       33 SCHEM 33.92469 0.9466555 0 MPRFLG 0 ; 
       34 SCHEM 77.00351 -17.69821 0 USR MPRFLG 0 ; 
       35 SCHEM 58.27762 -21.41134 0 MPRFLG 0 ; 
       36 SCHEM 75.75351 -21.69821 0 USR MPRFLG 0 ; 
       37 SCHEM 78.25351 -21.69821 0 USR MPRFLG 0 ; 
       38 SCHEM 37.6747 -3.053344 0 MPRFLG 0 ; 
       39 SCHEM 73.25351 -21.69821 0 USR MPRFLG 0 ; 
       40 SCHEM 70.75351 -21.69821 0 USR MPRFLG 0 ; 
       41 SCHEM 82.66441 -30.01663 0 MPRFLG 0 ; 
       42 SCHEM 50.17471 -1.05336 0 MPRFLG 0 ; 
       43 SCHEM 72.00351 -19.69821 0 USR MPRFLG 0 ; 
       44 SCHEM 17.67468 -1.05336 0 MPRFLG 0 ; 
       45 SCHEM 17.67468 -3.053344 0 MPRFLG 0 ; 
       46 SCHEM -11.20715 -18.82974 0 MPRFLG 0 ; 
       47 SCHEM -8.707152 -18.82974 0 MPRFLG 0 ; 
       48 SCHEM 30.77762 -23.41134 0 MPRFLG 0 ; 
       49 SCHEM 33.27762 -23.41134 0 MPRFLG 0 ; 
       50 SCHEM 35.77762 -23.41134 0 MPRFLG 0 ; 
       51 SCHEM 38.27762 -23.41134 0 MPRFLG 0 ; 
       52 SCHEM 40.77763 -23.41134 0 MPRFLG 0 ; 
       53 SCHEM 43.27761 -23.41134 0 MPRFLG 0 ; 
       54 SCHEM 45.77761 -23.41134 0 MPRFLG 0 ; 
       55 SCHEM 48.27762 -23.41134 0 MPRFLG 0 ; 
       56 SCHEM 50.77762 -23.41134 0 MPRFLG 0 ; 
       57 SCHEM 53.27762 -23.41134 0 MPRFLG 0 ; 
       58 SCHEM 55.77762 -23.41134 0 MPRFLG 0 ; 
       59 SCHEM 52.66442 -30.01663 0 MPRFLG 0 ; 
       60 SCHEM 52.66442 -32.01663 0 MPRFLG 0 ; 
       61 SCHEM 55.16442 -30.01663 0 MPRFLG 0 ; 
       62 SCHEM 55.16442 -32.01663 0 MPRFLG 0 ; 
       63 SCHEM 57.66442 -30.01663 0 MPRFLG 0 ; 
       64 SCHEM 57.66442 -32.01663 0 MPRFLG 0 ; 
       65 SCHEM 60.16441 -30.01663 0 MPRFLG 0 ; 
       66 SCHEM 60.16441 -32.01663 0 MPRFLG 0 ; 
       67 SCHEM 62.66441 -30.01663 0 MPRFLG 0 ; 
       68 SCHEM 62.66441 -32.01663 0 MPRFLG 0 ; 
       69 SCHEM 65.16441 -30.01663 0 MPRFLG 0 ; 
       70 SCHEM 65.16441 -32.01663 0 MPRFLG 0 ; 
       71 SCHEM 67.66441 -30.01663 0 MPRFLG 0 ; 
       72 SCHEM 67.66441 -32.01663 0 MPRFLG 0 ; 
       73 SCHEM 70.16442 -30.01663 0 MPRFLG 0 ; 
       74 SCHEM 70.16442 -32.01663 0 MPRFLG 0 ; 
       75 SCHEM 72.66441 -30.01663 0 MPRFLG 0 ; 
       76 SCHEM 72.66441 -32.01663 0 MPRFLG 0 ; 
       77 SCHEM 75.16441 -30.01663 0 MPRFLG 0 ; 
       78 SCHEM 75.16441 -32.01663 0 MPRFLG 0 ; 
       79 SCHEM 77.66441 -30.01663 0 MPRFLG 0 ; 
       80 SCHEM 77.66441 -32.01663 0 MPRFLG 0 ; 
       81 SCHEM 15.17468 -1.05336 0 MPRFLG 0 ; 
       82 SCHEM 15.17468 -3.053344 0 MPRFLG 0 ; 
       83 SCHEM 50.16708 -3.09082 0 USR MPRFLG 0 ; 
       84 SCHEM 16.29283 -18.82974 0 MPRFLG 0 ; 
       85 SCHEM 18.79283 -18.82974 0 MPRFLG 0 ; 
       86 SCHEM 13.79284 -20.82974 0 MPRFLG 0 ; 
       87 SCHEM 23.79283 -18.82974 0 MPRFLG 0 ; 
       88 SCHEM 52.6747 -3.053344 0 MPRFLG 0 ; 
       89 SCHEM 52.6747 -1.05336 0 MPRFLG 0 ; 
       90 SCHEM 40.77763 -21.41134 0 MPRFLG 0 ; 
       91 SCHEM 43.27761 -21.41134 0 MPRFLG 0 ; 
       92 SCHEM 45.77761 -21.41134 0 MPRFLG 0 ; 
       93 SCHEM 48.27762 -21.41134 0 MPRFLG 0 ; 
       94 SCHEM 50.77762 -21.41134 0 MPRFLG 0 ; 
       95 SCHEM 53.27762 -21.41134 0 MPRFLG 0 ; 
       96 SCHEM 55.77762 -21.41134 0 MPRFLG 0 ; 
       97 SCHEM 38.27762 -21.41134 0 MPRFLG 0 ; 
       98 SCHEM 35.77762 -21.41134 0 MPRFLG 0 ; 
       99 SCHEM 33.27762 -21.41134 0 MPRFLG 0 ; 
       100 SCHEM 30.77762 -21.41134 0 MPRFLG 0 ; 
       101 SCHEM 51.42471 0.9466555 0 MPRFLG 0 ; 
       102 SCHEM 77.00351 -19.69821 0 USR MPRFLG 0 ; 
       103 SCHEM 36.42469 4.946655 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       104 SCHEM 11.29284 -18.82974 0 MPRFLG 0 ; 
       105 SCHEM 40.1747 -1.05336 0 MPRFLG 0 ; 
       106 SCHEM 42.6747 -1.05336 0 MPRFLG 0 ; 
       107 SCHEM 45.17469 -1.05336 0 MPRFLG 0 ; 
       108 SCHEM 47.67471 -1.05336 0 MPRFLG 0 ; 
       109 SCHEM 65.17472 -1.05336 0 MPRFLG 0 ; 
       110 SCHEM 62.67473 -1.05336 0 MPRFLG 0 ; 
       111 SCHEM 60.17473 -1.05336 0 MPRFLG 0 ; 
       112 SCHEM 57.67473 -1.05336 0 MPRFLG 0 ; 
       113 SCHEM 67.66441 -28.01663 0 MPRFLG 0 ; 
       114 SCHEM 67.66441 -26.01663 0 USR MPRFLG 0 ; 
       115 SCHEM 45.77761 -17.41133 0 USR MPRFLG 0 ; 
       116 SCHEM 45.77761 -19.41134 0 MPRFLG 0 ; 
       117 SCHEM -9.957148 -16.82972 0 MPRFLG 0 ; 
       118 SCHEM 20.17468 -1.05336 0 MPRFLG 0 ; 
       119 SCHEM 22.67468 -1.05336 0 MPRFLG 0 ; 
       120 SCHEM 25.17468 -1.05336 0 MPRFLG 0 ; 
       121 SCHEM 22.67468 0.9466555 0 MPRFLG 0 ; 
       122 SCHEM 22.20885 -9.544127 0 USR MPRFLG 0 ; 
       123 SCHEM 23.4792 -10.51836 0 USR MPRFLG 0 ; 
       124 SCHEM 24.44164 -11.57492 0 USR MPRFLG 0 ; 
       125 SCHEM 34.70272 -10.22494 0 USR MPRFLG 0 ; 
       126 SCHEM 34.69342 -11.35486 0 USR MPRFLG 0 ; 
       127 SCHEM 34.5993 -12.24197 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 99.6236 -31.55383 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 98.73448 -33.82076 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 94.56268 -32.84214 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 164.7997 5.046949 0 WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 96.70243 -32.88737 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 102.7232 -31.1327 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 99.13117 -32.86156 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 106.905 -32.00999 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 101.5747 -33.08318 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 96.48994 -33.74059 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 106.2479 -33.92891 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 101.2472 -34.0144 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 104.1424 -33.06567 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 112.4041 -33.40808 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 106.7977 -33.13797 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 109.5253 -33.20991 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       16 SCHEM 103.5553 -33.88618 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       17 SCHEM 49.69205 -148.5291 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       18 SCHEM 76.35305 -146.1233 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 4.431778 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM -28.06822 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 327.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 451.25 -4 0 WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 104.4318 -43.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 216.9318 -45.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 206.9318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 214.4318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 243.1818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 238.1818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 11.93178 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM -20.56822 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 228.1818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 109.4318 -43.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 221.9318 -45.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 211.9318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 219.4318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 248.1818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 243.1818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 16.93179 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM -15.56821 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 233.1818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 199.4318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 96.93178 -43.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 206.9318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 220.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 322.5 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 325 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 453.75 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 111.9318 -43.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 224.4318 -45.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 214.4318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 221.9318 -51.82846 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 250.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 245.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 19.43179 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM -13.06821 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 235.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 99.43178 -43.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 211.9318 -45.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 201.9318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 209.4318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 238.1818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 233.1818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 6.931778 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM -25.56822 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 223.1818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 106.9318 -43.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 219.4318 -45.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 209.4318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 216.9318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 245.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 240.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 14.43179 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM -18.06821 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 230.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 103.8447 -36.28359 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 285 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 285 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 456.25 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 458.75 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 461.25 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 463.75 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 466.25 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       64 SCHEM 286.25 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       65 SCHEM 288.75 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       66 SCHEM 291.25 0 0 WIRECOL 10 7 MPRFLG 0 ; 
       67 SCHEM 471.25 5.046949 0 WIRECOL 10 7 MPRFLG 0 ; 
       68 SCHEM 209.4318 -45.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       69 SCHEM 235.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       70 SCHEM 230.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       71 SCHEM 101.9318 -43.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       72 SCHEM 214.4318 -45.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       73 SCHEM 204.4318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       74 SCHEM 211.9318 -51.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       75 SCHEM 240.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       76 SCHEM 235.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       77 SCHEM 9.431778 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       78 SCHEM -23.06822 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       79 SCHEM 225.6818 -47.77929 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 100 700 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
