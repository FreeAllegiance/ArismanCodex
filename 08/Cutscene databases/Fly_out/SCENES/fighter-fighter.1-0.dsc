SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       helmet-cam_int1.1-0 ROOT ; 
       helmet-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS NBELEM 1     
       fighter-Bionic_Lens1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 5     
       fighter-light2.1-0 ROOT ; 
       Vol-Engine1.1-0 ROOT ; 
       Vol-Engine2.1-0 ROOT ; 
       Vol-Engine3.1-0 ROOT ; 
       Vol-Engine4.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS NBELEM 4     
       fighter-engine1.1-0 ; 
       fighter-engine2.1-0 ; 
       fighter-engine3.1-0 ; 
       fighter-engine4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 12     
       fighter-Base_Green1.1-0 ; 
       fighter-Canopy1.1-0 ; 
       fighter-Dark1.1-0 ; 
       fighter-Engine_Vol1.1-0 ; 
       fighter-Light_Glass.1-0 ; 
       fighter-mat2.1-0 ; 
       fighter-mat3.1-0 ; 
       fighter-mat4.1-0 ; 
       fighter-mat5.1-0 ; 
       fighter-mat6.1-0 ; 
       fighter-METALS-chrome003.1-0.1-0 ; 
       fighter-WeatherStrip1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 82     
       fighter-BIG_CANOPY.1-0 ; 
       fighter-bottom20.1-0 ; 
       fighter-cube41.3-0 ; 
       fighter-cube45.1-0 ; 
       fighter-cube47.1-0 ; 
       fighter-cube48.1-0 ; 
       fighter-cube53.1-0 ; 
       fighter-cube54.1-0 ; 
       fighter-cube55.1-0 ; 
       fighter-cube56.1-0 ; 
       fighter-cube57.1-0 ; 
       fighter-cube58.1-0 ; 
       fighter-cube59.1-0 ; 
       fighter-engine_holder.1-0 ; 
       fighter-engine_holder1.1-0 ; 
       fighter-engine_holder3.1-0 ; 
       fighter-engine_holder4.1-0 ; 
       fighter-extru2.1-0 ; 
       fighter-extru57.1-0 ; 
       fighter-extru58.1-0 ; 
       fighter-extru60.1-0 ; 
       fighter-extru63.1-0 ; 
       fighter-extru7.1-0 ; 
       fighter-face1.1-0 ; 
       fighter-FUSELAGE_1.14-0 ; 
       fighter-grid1.1-0 ROOT ; 
       fighter-lamp.1-0 ; 
       fighter-lamp_1.1-0 ; 
       fighter-lamp_2.1-0 ; 
       fighter-lamp1.1-0 ; 
       fighter-light_area.1-0 ; 
       fighter-light_Area.1-0 ; 
       fighter-Light_Cover.1-0 ; 
       fighter-null11.1-0 ; 
       fighter-null12.7-0 ; 
       fighter-null13.1-0 ; 
       fighter-null2.1-0 ; 
       fighter-null7.1-0 ; 
       fighter-null8.1-0 ; 
       fighter-nurbs25.1-0 ; 
       fighter-nurbs27.1-0 ; 
       fighter-nurbs33.1-0 ; 
       fighter-nurbs39.1-0 ; 
       fighter-nurbs40.1-0 ; 
       fighter-revol110.1-0 ; 
       fighter-revol111.1-0 ; 
       fighter-revol112.1-0 ; 
       fighter-revol145.3-0 ; 
       fighter-revol146.1-0 ; 
       fighter-revol147.3-0 ; 
       fighter-revol148.1-0 ; 
       fighter-revol149.1-0 ; 
       fighter-revol150.1-0 ; 
       fighter-revol151.1-0 ; 
       fighter-revol152.1-0 ; 
       fighter-revol157.1-0 ; 
       fighter-revol158.1-0 ; 
       fighter-revol65.1-0 ; 
       fighter-revol68.1-0 ; 
       fighter-revol94.1-0 ; 
       fighter-revol99.1-0 ; 
       fighter-Root.1-0 ROOT ; 
       fighter-skin10.1-0 ; 
       fighter-skin11.1-0 ; 
       fighter-skin12.1-0 ; 
       fighter-skin13.1-0 ; 
       fighter-skin26.2-0 ; 
       fighter-skin27.1-0 ; 
       fighter-skin28.1-0 ; 
       fighter-skin29.1-0 ; 
       fighter-skin30.1-0 ; 
       fighter-skin4.1-0 ; 
       fighter-skin5.1-0 ; 
       fighter-skin6.1-0 ; 
       fighter-skin7.1-0 ; 
       fighter-Tail_Vol.1-0 ; 
       fighter-Target.1-0 ; 
       fighter-Target1.1-0 ; 
       fighter-Target2.1-0 ; 
       fighter-Target3.1-0 ; 
       fighter-wing_vol.1-0 ; 
       fighter-wing_vol1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 32     
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Bars ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Text ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Text2 ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Thrust_bump_lines ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Thrust_spec_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/Thrust_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/arrow ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/arrow2 ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/backgr ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/black ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/cubes ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/dotspic ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fin_bump ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fin_bump_lines ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fin_spec_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fin_tiles ; 
       //research/root/federation/shared_art_files/Cutscene/Production/models/Fighter/PICTURES/fuselage _Bump ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fuselage_bumpLines ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/fuselage_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/hazzard ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/intake_Soot ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/reflection ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/spec_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/specular_grain ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/tail_bump ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/tail_bumpLines ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/tail_spec_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/tail_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/thrust_bump ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/thrust_tiles ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/triangle_decal ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/triangle_decal2 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       fighter-fighter.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 64     
       fighter-arrow1.1-0 ; 
       fighter-arrow2.1-0 ; 
       fighter-Bars_Base1.1-0 ; 
       fighter-Bars1.1-0 ; 
       fighter-black.1-0 ; 
       fighter-black1.1-0 ; 
       fighter-black2.1-0 ; 
       fighter-black3.1-0 ; 
       fighter-bump_lines1.1-0 ; 
       fighter-bump_lines2.1-0 ; 
       fighter-bump_lines3.1-0 ; 
       fighter-bump_lines4.1-0 ; 
       fighter-bump_lines5.1-0 ; 
       fighter-bump_lines6.1-0 ; 
       fighter-Cubes1.1-0 ; 
       fighter-Cubes2.1-0 ; 
       fighter-Cubes3.1-0 ; 
       fighter-Cubes4.1-0 ; 
       fighter-Cubes5.1-0 ; 
       fighter-Cubes6.1-0 ; 
       fighter-dots1.1-0 ; 
       fighter-dots2.1-0 ; 
       fighter-Fuge_Bump.1-0 ; 
       fighter-Fuge_Bump1.1-0 ; 
       fighter-Fuge_Bump2.1-0 ; 
       fighter-Fuge_Bump3.1-0 ; 
       fighter-Fuge_Bump4.1-0 ; 
       fighter-Hazzard1.1-0 ; 
       fighter-Hazzard2.1-0 ; 
       fighter-Intake_Soot1.1-0 ; 
       fighter-nose1.1-0 ; 
       fighter-ref1.1-0 ; 
       fighter-reflection1.1-0 ; 
       fighter-Speck_Kill1.1-0 ; 
       fighter-specular_grain1.1-0 ; 
       fighter-specular_grain2.1-0 ; 
       fighter-specular_grain3.1-0 ; 
       fighter-specular_grain4.1-0 ; 
       fighter-specular_grain5.1-0 ; 
       fighter-specular_grain6.1-0 ; 
       fighter-Spec_Killer.1-0 ; 
       fighter-Spec_Killer1.1-0 ; 
       fighter-Spec_Killer2.1-0 ; 
       fighter-Spec_Killer3.1-0 ; 
       fighter-Spec_Killer4.1-0 ; 
       fighter-Spec_Killer5.1-0 ; 
       fighter-spec_tiles1.1-0 ; 
       fighter-spec_tiles2.1-0 ; 
       fighter-spec_tiles3.1-0 ; 
       fighter-spec_tiles4.1-0 ; 
       fighter-spec_tiles5.1-0 ; 
       fighter-spec_tiles6.1-0 ; 
       fighter-t2d1.1-0 ; 
       fighter-tail_Bump1.1-0 ; 
       fighter-text1.1-0 ; 
       fighter-text2.1-0 ; 
       fighter-Tiles1.1-0 ; 
       fighter-Tiles2.1-0 ; 
       fighter-Tiles3.1-0 ; 
       fighter-Tiles4.1-0 ; 
       fighter-Tiles5.1-0 ; 
       fighter-Tiles6.1-0 ; 
       fighter-triangle1.1-0 ; 
       fighter-triangle2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS NBELEM 1     
       fighter-Engine_Volume1.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 19 110 ; 
       1 37 110 ; 
       2 5 110 ; 
       3 2 110 ; 
       4 2 110 ; 
       5 17 110 ; 
       6 74 110 ; 
       7 66 110 ; 
       8 17 110 ; 
       9 8 110 ; 
       10 9 110 ; 
       11 9 110 ; 
       12 24 110 ; 
       13 60 110 ; 
       14 44 110 ; 
       15 56 110 ; 
       16 55 110 ; 
       17 34 110 ; 
       18 37 110 ; 
       19 24 110 ; 
       20 74 110 ; 
       21 9 110 ; 
       22 2 110 ; 
       23 17 110 ; 
       24 61 110 ; 
       26 30 110 ; 
       27 30 110 ; 
       28 31 110 ; 
       29 31 110 ; 
       30 32 110 ; 
       31 32 110 ; 
       32 24 110 ; 
       33 34 110 ; 
       34 24 110 ; 
       35 24 110 ; 
       36 24 110 ; 
       37 24 110 ; 
       38 34 110 ; 
       39 60 110 ; 
       40 60 110 ; 
       41 0 110 ; 
       42 44 110 ; 
       43 44 110 ; 
       44 33 110 ; 
       45 33 110 ; 
       46 45 110 ; 
       47 6 110 ; 
       48 47 110 ; 
       49 6 110 ; 
       50 49 110 ; 
       51 58 110 ; 
       52 45 110 ; 
       53 12 110 ; 
       54 12 110 ; 
       55 50 110 ; 
       56 48 110 ; 
       57 58 110 ; 
       58 38 110 ; 
       59 34 110 ; 
       60 38 110 ; 
       62 36 110 ; 
       63 36 110 ; 
       64 36 110 ; 
       65 36 110 ; 
       66 17 110 ; 
       67 35 110 ; 
       68 35 110 ; 
       69 35 110 ; 
       70 35 110 ; 
       71 74 110 ; 
       72 74 110 ; 
       73 74 110 ; 
       74 17 110 ; 
       75 61 110 ; 
       76 27 110 ; 
       77 26 110 ; 
       78 29 110 ; 
       79 28 110 ; 
       80 61 110 ; 
       81 61 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       2 0 300 ; 
       3 0 300 ; 
       4 0 300 ; 
       5 0 300 ; 
       6 10 300 ; 
       8 0 300 ; 
       9 0 300 ; 
       10 0 300 ; 
       11 0 300 ; 
       17 0 300 ; 
       19 11 300 ; 
       20 8 300 ; 
       21 0 300 ; 
       22 0 300 ; 
       24 0 300 ; 
       26 10 300 ; 
       27 10 300 ; 
       28 10 300 ; 
       29 10 300 ; 
       30 2 300 ; 
       31 2 300 ; 
       32 4 300 ; 
       35 10 300 ; 
       36 10 300 ; 
       39 5 300 ; 
       40 7 300 ; 
       41 0 300 ; 
       42 6 300 ; 
       43 5 300 ; 
       44 0 300 ; 
       47 10 300 ; 
       48 9 300 ; 
       49 10 300 ; 
       50 9 300 ; 
       55 9 300 ; 
       56 9 300 ; 
       60 0 300 ; 
       62 10 300 ; 
       63 10 300 ; 
       64 10 300 ; 
       65 10 300 ; 
       67 10 300 ; 
       68 10 300 ; 
       69 10 300 ; 
       70 10 300 ; 
       71 8 300 ; 
       72 8 300 ; 
       73 8 300 ; 
       74 2 300 ; 
       75 3 300 ; 
       80 3 300 ; 
       81 3 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 32 400 ; 
       17 53 400 ; 
       17 41 400 ; 
       17 57 400 ; 
       17 9 400 ; 
       17 47 400 ; 
       17 15 400 ; 
       17 35 400 ; 
       17 54 400 ; 
       17 55 400 ; 
       21 26 400 ; 
       21 45 400 ; 
       21 61 400 ; 
       21 13 400 ; 
       21 51 400 ; 
       21 19 400 ; 
       21 37 400 ; 
       22 25 400 ; 
       22 44 400 ; 
       22 60 400 ; 
       22 12 400 ; 
       22 50 400 ; 
       22 18 400 ; 
       22 36 400 ; 
       24 22 400 ; 
       24 40 400 ; 
       24 56 400 ; 
       24 8 400 ; 
       24 46 400 ; 
       24 29 400 ; 
       24 14 400 ; 
       24 34 400 ; 
       24 20 400 ; 
       24 21 400 ; 
       24 30 400 ; 
       24 62 400 ; 
       24 0 400 ; 
       24 1 400 ; 
       24 63 400 ; 
       41 33 400 ; 
       41 3 400 ; 
       41 2 400 ; 
       44 23 400 ; 
       44 42 400 ; 
       44 58 400 ; 
       44 10 400 ; 
       44 48 400 ; 
       44 16 400 ; 
       44 28 400 ; 
       44 6 400 ; 
       44 39 400 ; 
       47 7 400 ; 
       49 4 400 ; 
       60 24 400 ; 
       60 43 400 ; 
       60 59 400 ; 
       60 11 400 ; 
       60 49 400 ; 
       60 17 400 ; 
       60 27 400 ; 
       60 5 400 ; 
       60 38 400 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER MODELS 
       1 14 2111 ; 
       2 13 2111 ; 
       3 15 2111 ; 
       4 16 2111 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       10 52 401 ; 
       10 31 401 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER VOLUME_SHADERS 
       3 0 550 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS CHAPTER LIGHT_SHADERS 
       1 0 550 ; 
       2 1 550 ; 
       3 2 550 ; 
       4 3 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER CAMERA_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS CHAPTER LIGHTS 
       0 1 551 1 ; 
       0 2 551 1 ; 
       0 3 551 1 ; 
       0 4 551 1 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS CHAPTER LIGHTS 
       0 1 551 1 ; 
       0 2 551 1 ; 
       0 3 551 1 ; 
       0 4 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 120 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       1 SCHEM 117.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       2 SCHEM 122.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       3 SCHEM 125 0 0 WIRECOL 7 7 MPRFLG 0 ; 
       4 SCHEM 127.5 0 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM 70 -6 0 MPRFLG 0 ; 
       1 SCHEM 5 -6 0 MPRFLG 0 ; 
       2 SCHEM 62.5 -10 0 MPRFLG 0 ; 
       3 SCHEM 62.5 -12 0 MPRFLG 0 ; 
       4 SCHEM 65 -12 0 MPRFLG 0 ; 
       5 SCHEM 62.5 -8 0 MPRFLG 0 ; 
       6 SCHEM 48.75 -10 0 MPRFLG 0 ; 
       7 SCHEM 35 -10 0 MPRFLG 0 ; 
       8 SCHEM 55 -8 0 MPRFLG 0 ; 
       9 SCHEM 55 -10 0 MPRFLG 0 ; 
       10 SCHEM 55 -12 0 MPRFLG 0 ; 
       11 SCHEM 57.5 -12 0 MPRFLG 0 ; 
       12 SCHEM 93.75 -4 0 MPRFLG 0 ; 
       13 SCHEM 25 -10 0 WIRECOL 4 7 DISPLAY 1 2 MPRFLG 0 ; 
       14 SCHEM 17.5 -10 0 WIRECOL 4 7 MPRFLG 0 ; 
       15 SCHEM 47.5 -18 0 WIRECOL 4 7 MPRFLG 0 ; 
       16 SCHEM 50 -18 0 WIRECOL 4 7 MPRFLG 0 ; 
       17 SCHEM 48.75 -6 0 MPRFLG 0 ; 
       18 SCHEM 2.5 -6 0 MPRFLG 0 ; 
       19 SCHEM 70 -4 0 MPRFLG 0 ; 
       20 SCHEM 45 -10 0 MPRFLG 0 ; 
       21 SCHEM 52.5 -12 0 MPRFLG 0 ; 
       22 SCHEM 60 -12 0 MPRFLG 0 ; 
       23 SCHEM 32.5 -8 0 MPRFLG 0 ; 
       24 SCHEM 53.75 -2 0 MPRFLG 0 ; 
       25 SCHEM 115 0 0 DISPLAY 0 0 SRT 14.85 14.85 14.85 0 0 0 0 -5.05627 0 MPRFLG 0 ; 
       26 SCHEM 102.5 -8 0 MPRFLG 0 ; 
       27 SCHEM 105 -8 0 MPRFLG 0 ; 
       28 SCHEM 100 -8 0 MPRFLG 0 ; 
       29 SCHEM 97.5 -8 0 MPRFLG 0 ; 
       30 SCHEM 103.75 -6 0 MPRFLG 0 ; 
       31 SCHEM 98.75 -6 0 MPRFLG 0 ; 
       32 SCHEM 101.25 -4 0 MPRFLG 0 ; 
       33 SCHEM 12.5 -6 0 MPRFLG 0 ; 
       34 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       35 SCHEM 86.25 -4 0 MPRFLG 0 ; 
       36 SCHEM 76.25 -4 0 MPRFLG 0 ; 
       37 SCHEM 3.75 -4 0 MPRFLG 0 ; 
       38 SCHEM 25 -6 0 MPRFLG 0 ; 
       39 SCHEM 20 -10 0 MPRFLG 0 ; 
       40 SCHEM 22.5 -10 0 MPRFLG 0 ; 
       41 SCHEM 70 -8 0 MPRFLG 0 ; 
       42 SCHEM 15 -10 0 MPRFLG 0 ; 
       43 SCHEM 12.5 -10 0 MPRFLG 0 ; 
       44 SCHEM 15 -8 0 MPRFLG 0 ; 
       45 SCHEM 8.75 -8 0 MPRFLG 0 ; 
       46 SCHEM 10 -10 0 MPRFLG 0 ; 
       47 SCHEM 47.5 -12 0 MPRFLG 0 ; 
       48 SCHEM 47.5 -14 0 MPRFLG 0 ; 
       49 SCHEM 50 -12 0 MPRFLG 0 ; 
       50 SCHEM 50 -14 0 MPRFLG 0 ; 
       51 SCHEM 30 -10 0 MPRFLG 0 ; 
       52 SCHEM 7.5 -10 0 MPRFLG 0 ; 
       53 SCHEM 92.5 -6 0 MPRFLG 0 ; 
       54 SCHEM 95 -6 0 MPRFLG 0 ; 
       55 SCHEM 50 -16 0 MPRFLG 0 ; 
       56 SCHEM 47.5 -16 0 MPRFLG 0 ; 
       57 SCHEM 27.5 -10 0 MPRFLG 0 ; 
       58 SCHEM 28.75 -8 0 MPRFLG 0 ; 
       59 SCHEM 67.5 -6 0 MPRFLG 0 ; 
       60 SCHEM 22.5 -8 0 MPRFLG 0 ; 
       61 SCHEM 57.5 0 0 SRT 1 1 1 3.293098e-007 0 2.256783e-007 0 0 0 MPRFLG 0 ; 
       62 SCHEM 72.5 -6 0 MPRFLG 0 ; 
       63 SCHEM 75 -6 0 MPRFLG 0 ; 
       64 SCHEM 77.5 -6 0 MPRFLG 0 ; 
       65 SCHEM 80 -6 0 MPRFLG 0 ; 
       66 SCHEM 35 -8 0 MPRFLG 0 ; 
       67 SCHEM 90 -6 0 MPRFLG 0 ; 
       68 SCHEM 87.5 -6 0 MPRFLG 0 ; 
       69 SCHEM 85 -6 0 MPRFLG 0 ; 
       70 SCHEM 82.5 -6 0 MPRFLG 0 ; 
       71 SCHEM 37.5 -10 0 MPRFLG 0 ; 
       72 SCHEM 40 -10 0 MPRFLG 0 ; 
       73 SCHEM 42.5 -10 0 MPRFLG 0 ; 
       74 SCHEM 43.75 -8 0 MPRFLG 0 ; 
       75 SCHEM 107.5 -2 0 MPRFLG 0 ; 
       76 SCHEM 105 -10 0 MPRFLG 0 ; 
       77 SCHEM 102.5 -10 0 MPRFLG 0 ; 
       78 SCHEM 97.5 -10 0 MPRFLG 0 ; 
       79 SCHEM 100 -10 0 MPRFLG 0 ; 
       80 SCHEM 110 -2 0 MPRFLG 0 ; 
       81 SCHEM 112.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 128.6797 -22.5756 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 96.8251 -23.5738 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 76.86731 -26.61387 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 111.5054 -4.789365 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 86.4373 -24.31424 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 186.4317 -23.88084 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 209.9715 -25.12737 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 190.389 -24.92353 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 162.6538 -28.87474 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 211.3315 -40.24283 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 167.6029 -23.6404 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 93.4084 -23.51319 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 97.58151 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 100.0815 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 91.0643 -43.07325 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 91.0034 -45.28896 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 166.8608 -31.31507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 179.5186 -26.95964 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 204.5186 -26.95964 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 173.1108 -31.31507 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 65.62741 -44.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 133.3363 -40.92917 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 186.044 -44.11007 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 168.544 -44.11007 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 63.12741 -54.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 70.62741 -54.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 68.10991 -43.25944 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 135.8188 -40.15203 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 188.5266 -43.33293 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 171.0266 -43.33293 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 65.61 -53.25944 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 73.10999 -53.25944 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 90.08151 -42.11604 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 89.8569 -43.18038 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 70.41661 -43.20327 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 190.8332 -43.27677 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 173.7614 -47.83854 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 67.91661 -53.20327 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 75.4166 -53.20327 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 173.3002 -40.83085 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 198.3002 -40.83085 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 70.62741 -44.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 92.58151 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 167.1314 -25.76207 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 94.9557 -45.97788 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 93.77271 -48.45427 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 71.9855 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 143.2355 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 143.2355 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 160.7355 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 196.9855 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 221.9855 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 60.62741 -44.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       41 SCHEM 128.3363 -40.92917 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       42 SCHEM 181.044 -44.11007 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       43 SCHEM 163.544 -44.11007 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       44 SCHEM 58.12741 -54.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       45 SCHEM 65.62741 -54.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       46 SCHEM 68.12741 -44.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       47 SCHEM 135.8363 -40.92917 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       48 SCHEM 188.544 -44.11007 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       49 SCHEM 171.044 -44.11007 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       50 SCHEM 65.62741 -54.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       51 SCHEM 73.12741 -54.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       52 SCHEM 168.8298 -25.02324 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       53 SCHEM 138.1254 -40.09586 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       54 SCHEM 171.3315 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       55 SCHEM 173.8315 -40.24283 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       56 SCHEM 63.12741 -44.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       57 SCHEM 130.8363 -40.92917 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       58 SCHEM 183.544 -44.11007 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       59 SCHEM 166.044 -44.11007 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       60 SCHEM 60.62741 -54.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       61 SCHEM 68.12741 -54.03658 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       62 SCHEM 101.1781 -36.16895 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       63 SCHEM 95.44551 -36.20693 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER VOLUME_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHT_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
       2 SCHEM 0 0 0 ; 
       3 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERA_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 30 30 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
