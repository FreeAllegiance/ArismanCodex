SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       Bomber2-Bom_root.31-0 ; 
       Fig_root-Fig_Root.34-0 ; 
       Fig_root3-Fig_Root.30-0 ; 
       Platform-Root.44-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.39-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       strobes-mat448.3-0 ; 
       strobes-mat449.3-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 306     
       Bomber2-BIG_CANOPY.1-0 ; 
       Bomber2-Bom_root.28-0 ROOT ; 
       Bomber2-bottom20.1-0 ; 
       Bomber2-cube41.3-0 ; 
       Bomber2-cube42.1-0 ; 
       Bomber2-cube44.1-0 ; 
       Bomber2-cube45.1-0 ; 
       Bomber2-cube47.1-0 ; 
       Bomber2-cube48.1-0 ; 
       Bomber2-cube49.1-0 ; 
       Bomber2-cube50.1-0 ; 
       Bomber2-cube51.1-0 ; 
       Bomber2-cube52.1-0 ; 
       Bomber2-cube53.1-0 ; 
       Bomber2-extru2.1-0 ; 
       Bomber2-extru42.1-0 ; 
       Bomber2-extru53.1-0 ; 
       Bomber2-extru57.1-0 ; 
       Bomber2-extru58.1-0 ; 
       Bomber2-extru60.1-0 ; 
       Bomber2-extru7.1-0 ; 
       Bomber2-face1.1-0 ; 
       Bomber2-face2.1-0 ; 
       Bomber2-FUSELAGE.1-0 ; 
       Bomber2-light_holder_1.1-0 ; 
       Bomber2-light_holder_2.1-0 ; 
       Bomber2-light_holder_3.1-0 ; 
       Bomber2-light_holder_4.1-0 ; 
       Bomber2-lwingzz1.1-0 ; 
       Bomber2-null11.1-0 ; 
       Bomber2-null12.1-0 ; 
       Bomber2-null2.1-0 ; 
       Bomber2-null3.1-0 ; 
       Bomber2-null7.1-0 ; 
       Bomber2-null8.1-0 ; 
       Bomber2-nurbs22.1-0 ; 
       Bomber2-nurbs25.1-0 ; 
       Bomber2-nurbs27.1-0 ; 
       Bomber2-nurbs33.1-0 ; 
       Bomber2-nurbs37.1-0 ; 
       Bomber2-nurbs38.1-0 ; 
       Bomber2-nurbs8.1-0 ; 
       Bomber2-revol100.1-0 ; 
       Bomber2-revol102.1-0 ; 
       Bomber2-revol107.1-0 ; 
       Bomber2-revol108.1-0 ; 
       Bomber2-revol110.1-0 ; 
       Bomber2-revol111.1-0 ; 
       Bomber2-revol112.1-0 ; 
       Bomber2-revol113.1-0 ; 
       Bomber2-revol114.1-0 ; 
       Bomber2-revol115.1-0 ; 
       Bomber2-revol116.1-0 ; 
       Bomber2-revol117.1-0 ; 
       Bomber2-revol118.1-0 ; 
       Bomber2-revol119.1-0 ; 
       Bomber2-revol120.1-0 ; 
       Bomber2-revol121.1-0 ; 
       Bomber2-revol122.1-0 ; 
       Bomber2-revol123.1-0 ; 
       Bomber2-revol124.1-0 ; 
       Bomber2-revol125.1-0 ; 
       Bomber2-revol126.1-0 ; 
       Bomber2-revol127.1-0 ; 
       Bomber2-revol128.1-0 ; 
       Bomber2-revol129.1-0 ; 
       Bomber2-revol130.1-0 ; 
       Bomber2-revol131.1-0 ; 
       Bomber2-revol132.1-0 ; 
       Bomber2-revol133.1-0 ; 
       Bomber2-revol134.1-0 ; 
       Bomber2-revol135.1-0 ; 
       Bomber2-revol136.1-0 ; 
       Bomber2-revol137.1-0 ; 
       Bomber2-revol138.1-0 ; 
       Bomber2-revol139.1-0 ; 
       Bomber2-revol140.1-0 ; 
       Bomber2-revol141.1-0 ; 
       Bomber2-revol142.1-0 ; 
       Bomber2-revol143.1-0 ; 
       Bomber2-revol144.1-0 ; 
       Bomber2-revol145.1-0 ; 
       Bomber2-revol146.1-0 ; 
       Bomber2-revol147.1-0 ; 
       Bomber2-revol57.1-0 ; 
       Bomber2-revol58.1-0 ; 
       Bomber2-revol59.1-0 ; 
       Bomber2-revol60.1-0 ; 
       Bomber2-revol65.1-0 ; 
       Bomber2-revol68.1-0 ; 
       Bomber2-revol82.1-0 ; 
       Bomber2-revol83.1-0 ; 
       Bomber2-revol84.1-0 ; 
       Bomber2-revol85.1-0 ; 
       Bomber2-revol86.1-0 ; 
       Bomber2-revol87.1-0 ; 
       Bomber2-revol88.1-0 ; 
       Bomber2-revol89.1-0 ; 
       Bomber2-revol90.1-0 ; 
       Bomber2-revol91.1-0 ; 
       Bomber2-revol92.1-0 ; 
       Bomber2-revol94.1-0 ; 
       Bomber2-revol99.1-0 ; 
       Bomber2-Root.1-0 ; 
       Bomber2-rwingzz1.1-0 ; 
       Bomber2-skin10.1-0 ; 
       Bomber2-skin11.1-0 ; 
       Bomber2-skin12.1-0 ; 
       Bomber2-skin13.1-0 ; 
       Bomber2-skin14.1-0 ; 
       Bomber2-skin15.1-0 ; 
       Bomber2-skin16.1-0 ; 
       Bomber2-skin17.1-0 ; 
       Bomber2-skin19.1-0 ; 
       Bomber2-skin20.1-0 ; 
       Bomber2-skin23.1-0 ; 
       Bomber2-skin24.1-0 ; 
       Bomber2-skin26.2-0 ; 
       Bomber2-skin4.1-0 ; 
       Bomber2-skin5.1-0 ; 
       Bomber2-skin6.1-0 ; 
       Bomber2-skin7.1-0 ; 
       Bomber2-strobe_GN_1.1-0 ; 
       Bomber2-strobe_GN_2.1-0 ; 
       Bomber2-strobe_GN_3.1-0 ; 
       Bomber2-strobe_RD_1.1-0 ; 
       Bomber2-strobe_RD_2.1-0 ; 
       Bomber2-strobe_RD_3.1-0 ; 
       Fig_root-BIG_CANOPY.1-0 ; 
       Fig_root-bottom20.1-0 ; 
       Fig_root-cube41.3-0 ; 
       Fig_root-cube45.1-0 ; 
       Fig_root-cube47.1-0 ; 
       Fig_root-cube48.1-0 ; 
       Fig_root-cube53.1-0 ; 
       Fig_root-cube54.1-0 ; 
       Fig_root-cube55.1-0 ; 
       Fig_root-cube56.1-0 ; 
       Fig_root-cube57.1-0 ; 
       Fig_root-cube58.1-0 ; 
       Fig_root-cube59.1-0 ; 
       Fig_root-engine_holder.1-0 ; 
       Fig_root-engine_holder1.1-0 ; 
       Fig_root-engine_holder3.1-0 ; 
       Fig_root-engine_holder4.1-0 ; 
       Fig_root-extru2.1-0 ; 
       Fig_root-extru57.1-0 ; 
       Fig_root-extru58.1-0 ; 
       Fig_root-extru60.1-0 ; 
       Fig_root-extru63.1-0 ; 
       Fig_root-extru7.1-0 ; 
       Fig_root-face1.1-0 ; 
       Fig_root-Fig_Root.31-0 ROOT ; 
       Fig_root-FUSELAGE_1.14-0 ; 
       Fig_root-lamp.1-0 ; 
       Fig_root-lamp_1.1-0 ; 
       Fig_root-lamp_2.1-0 ; 
       Fig_root-lamp1.1-0 ; 
       Fig_root-light_area.1-0 ; 
       Fig_root-light_Area.1-0 ; 
       Fig_root-Light_Cover.1-0 ; 
       Fig_root-null11.1-0 ; 
       Fig_root-null12.7-0 ; 
       Fig_root-null13.1-0 ; 
       Fig_root-null2.1-0 ; 
       Fig_root-null241.1-0 ; 
       Fig_root-null7.1-0 ; 
       Fig_root-null8.1-0 ; 
       Fig_root-nurbs25.1-0 ; 
       Fig_root-nurbs27.1-0 ; 
       Fig_root-nurbs33.1-0 ; 
       Fig_root-nurbs39.1-0 ; 
       Fig_root-nurbs40.1-0 ; 
       Fig_root-revol110.1-0 ; 
       Fig_root-revol111.1-0 ; 
       Fig_root-revol112.1-0 ; 
       Fig_root-revol145.3-0 ; 
       Fig_root-revol146.1-0 ; 
       Fig_root-revol147.3-0 ; 
       Fig_root-revol148.1-0 ; 
       Fig_root-revol149.1-0 ; 
       Fig_root-revol150.1-0 ; 
       Fig_root-revol151.1-0 ; 
       Fig_root-revol152.1-0 ; 
       Fig_root-revol157.1-0 ; 
       Fig_root-revol158.1-0 ; 
       Fig_root-revol65.1-0 ; 
       Fig_root-revol68.1-0 ; 
       Fig_root-revol94.1-0 ; 
       Fig_root-revol99.1-0 ; 
       Fig_root-Root.4-0 ; 
       Fig_root-skin10.1-0 ; 
       Fig_root-skin11.1-0 ; 
       Fig_root-skin12.1-0 ; 
       Fig_root-skin13.1-0 ; 
       Fig_root-skin26.2-0 ; 
       Fig_root-skin27.1-0 ; 
       Fig_root-skin28.1-0 ; 
       Fig_root-skin29.1-0 ; 
       Fig_root-skin30.1-0 ; 
       Fig_root-skin4.1-0 ; 
       Fig_root-skin5.1-0 ; 
       Fig_root-skin6.1-0 ; 
       Fig_root-skin7.1-0 ; 
       Fig_root-strobe_GN_7.1-0 ; 
       Fig_root-strobe_GN_8.1-0 ; 
       Fig_root-strobe_GN_9.1-0 ; 
       Fig_root-strobe_RD_7.1-0 ; 
       Fig_root-strobe_RD_8.1-0 ; 
       Fig_root-strobe_RD_9.1-0 ; 
       Fig_root-Target.1-0 ; 
       Fig_root-Target1.1-0 ; 
       Fig_root-Target2.1-0 ; 
       Fig_root-Target3.1-0 ; 
       Fig_root3-BIG_CANOPY1.1-0 ; 
       Fig_root3-bottom30.1-0 ; 
       Fig_root3-cube544.1-0 ; 
       Fig_root3-cube545.1-0 ; 
       Fig_root3-cube546.1-0 ; 
       Fig_root3-cube547.1-0 ; 
       Fig_root3-cube548.1-0 ; 
       Fig_root3-cube549.1-0 ; 
       Fig_root3-cube550.1-0 ; 
       Fig_root3-cube551.1-0 ; 
       Fig_root3-cube552.1-0 ; 
       Fig_root3-cube553.1-0 ; 
       Fig_root3-cube554.1-0 ; 
       Fig_root3-engine_holder5.1-0 ; 
       Fig_root3-engine_holder6.1-0 ; 
       Fig_root3-engine_holder7.1-0 ; 
       Fig_root3-engine_holder8.1-0 ; 
       Fig_root3-extru299.1-0 ; 
       Fig_root3-extru300.1-0 ; 
       Fig_root3-extru301.1-0 ; 
       Fig_root3-extru302.1-0 ; 
       Fig_root3-extru303.1-0 ; 
       Fig_root3-extru304.1-0 ; 
       Fig_root3-face48.1-0 ; 
       Fig_root3-Fig_Root.23-0 ROOT ; 
       Fig_root3-FUSELAGE_2.1-0 ; 
       Fig_root3-lamp_3.1-0 ; 
       Fig_root3-lamp_4.1-0 ; 
       Fig_root3-lamp2.1-0 ; 
       Fig_root3-lamp3.1-0 ; 
       Fig_root3-light_Area1.1-0 ; 
       Fig_root3-light_area1.1-0 ; 
       Fig_root3-Light_Cover1.1-0 ; 
       Fig_root3-null234.1-0 ; 
       Fig_root3-null235.1-0 ; 
       Fig_root3-null236.1-0 ; 
       Fig_root3-null237.1-0 ; 
       Fig_root3-null238.1-0 ; 
       Fig_root3-null239.1-0 ; 
       Fig_root3-null240.1-0 ; 
       Fig_root3-nurbs136.1-0 ; 
       Fig_root3-nurbs137.1-0 ; 
       Fig_root3-nurbs138.1-0 ; 
       Fig_root3-nurbs139.1-0 ; 
       Fig_root3-nurbs140.1-0 ; 
       Fig_root3-revol180.1-0 ; 
       Fig_root3-revol181.1-0 ; 
       Fig_root3-revol182.1-0 ; 
       Fig_root3-revol183.1-0 ; 
       Fig_root3-revol184.1-0 ; 
       Fig_root3-revol185.1-0 ; 
       Fig_root3-revol186.1-0 ; 
       Fig_root3-revol187.1-0 ; 
       Fig_root3-revol188.1-0 ; 
       Fig_root3-revol189.1-0 ; 
       Fig_root3-revol190.1-0 ; 
       Fig_root3-revol191.1-0 ; 
       Fig_root3-revol192.1-0 ; 
       Fig_root3-revol193.1-0 ; 
       Fig_root3-revol194.1-0 ; 
       Fig_root3-revol195.1-0 ; 
       Fig_root3-revol196.1-0 ; 
       Fig_root3-Root1.1-0 ; 
       Fig_root3-skin31.1-0 ; 
       Fig_root3-skin32.1-0 ; 
       Fig_root3-skin33.1-0 ; 
       Fig_root3-skin34.1-0 ; 
       Fig_root3-skin35.1-0 ; 
       Fig_root3-skin36.1-0 ; 
       Fig_root3-skin37.1-0 ; 
       Fig_root3-skin38.1-0 ; 
       Fig_root3-skin39.1-0 ; 
       Fig_root3-skin40.1-0 ; 
       Fig_root3-skin41.1-0 ; 
       Fig_root3-skin42.1-0 ; 
       Fig_root3-skin43.1-0 ; 
       Fig_root3-strobe_GN_4.1-0 ; 
       Fig_root3-strobe_GN_5.1-0 ; 
       Fig_root3-strobe_GN_6.1-0 ; 
       Fig_root3-strobe_RD_4.1-0 ; 
       Fig_root3-strobe_RD_5.1-0 ; 
       Fig_root3-strobe_RD_6.1-0 ; 
       Fig_root3-Target4.1-0 ; 
       Fig_root3-Target5.1-0 ; 
       Fig_root3-Target6.1-0 ; 
       Fig_root3-Target7.1-0 ; 
       lighting_ships-Fighter_cam_Path.6-0 ROOT ; 
       Platform-Cam.1-0 ; 
       Platform-Int.1-0 ; 
       Platform-Root.42-0 ROOT ; 
       Set1-Fig2_Path.10-0 ROOT ; 
       Set2-Fighter_cam_Path.11-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 1     
       re_animate_Strobes-strobes1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       strobes-re-animate_Strobes.4-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 18 110 ; 
       1 305 112 ; 
       1 305 112 2 ; 
       1 305 124 ; 
       1 305 124 2 ; 
       2 33 110 ; 
       3 8 110 ; 
       4 113 110 ; 
       5 116 110 ; 
       6 3 110 ; 
       7 3 110 ; 
       8 14 110 ; 
       9 14 110 ; 
       10 9 110 ; 
       11 10 110 ; 
       12 10 110 ; 
       13 121 110 ; 
       14 23 110 ; 
       15 14 110 ; 
       16 10 110 ; 
       17 33 110 ; 
       18 23 110 ; 
       19 121 110 ; 
       20 3 110 ; 
       21 14 110 ; 
       22 15 110 ; 
       23 103 110 ; 
       24 23 110 ; 
       25 23 110 ; 
       26 23 110 ; 
       27 23 110 ; 
       28 15 110 ; 
       29 23 110 ; 
       30 103 110 ; 
       31 23 110 ; 
       32 23 110 ; 
       33 23 110 ; 
       34 23 110 ; 
       35 116 110 ; 
       36 102 110 ; 
       37 102 110 ; 
       38 0 110 ; 
       39 43 110 ; 
       40 43 110 ; 
       41 113 110 ; 
       42 101 110 ; 
       43 29 110 ; 
       44 121 110 ; 
       45 44 110 ; 
       46 117 110 ; 
       47 117 110 ; 
       48 100 110 ; 
       49 99 110 ; 
       50 98 110 ; 
       51 97 110 ; 
       52 90 110 ; 
       53 91 110 ; 
       54 92 110 ; 
       55 93 110 ; 
       56 94 110 ; 
       57 95 110 ; 
       58 96 110 ; 
       59 113 110 ; 
       60 59 110 ; 
       61 113 110 ; 
       62 61 110 ; 
       63 113 110 ; 
       64 63 110 ; 
       65 113 110 ; 
       66 65 110 ; 
       67 113 110 ; 
       68 67 110 ; 
       69 113 110 ; 
       70 69 110 ; 
       71 113 110 ; 
       72 71 110 ; 
       73 113 110 ; 
       74 73 110 ; 
       75 113 110 ; 
       76 75 110 ; 
       77 113 110 ; 
       78 77 110 ; 
       79 113 110 ; 
       80 79 110 ; 
       81 121 110 ; 
       82 81 110 ; 
       83 42 110 ; 
       84 15 110 ; 
       85 15 110 ; 
       86 28 110 ; 
       87 15 110 ; 
       88 89 110 ; 
       89 101 110 ; 
       90 116 110 ; 
       91 116 110 ; 
       92 116 110 ; 
       93 116 110 ; 
       94 116 110 ; 
       95 116 110 ; 
       96 116 110 ; 
       97 116 110 ; 
       98 116 110 ; 
       99 116 110 ; 
       100 116 110 ; 
       101 23 110 ; 
       102 34 110 ; 
       103 1 110 ; 
       104 15 110 ; 
       105 31 110 ; 
       106 31 110 ; 
       107 31 110 ; 
       108 31 110 ; 
       109 32 110 ; 
       110 32 110 ; 
       111 32 110 ; 
       112 32 110 ; 
       113 114 110 ; 
       114 23 110 ; 
       115 23 110 ; 
       116 115 110 ; 
       117 14 110 ; 
       118 121 110 ; 
       119 121 110 ; 
       120 121 110 ; 
       121 23 110 ; 
       122 30 110 ; 
       123 30 110 ; 
       124 30 110 ; 
       125 30 110 ; 
       126 30 110 ; 
       127 30 110 ; 
       128 147 110 ; 
       129 166 110 ; 
       130 133 110 ; 
       131 130 110 ; 
       132 130 110 ; 
       133 145 110 ; 
       134 203 110 ; 
       135 195 110 ; 
       136 145 110 ; 
       137 136 110 ; 
       138 137 110 ; 
       139 137 110 ; 
       140 153 110 ; 
       141 189 110 ; 
       142 173 110 ; 
       143 185 110 ; 
       144 184 110 ; 
       145 162 110 ; 
       146 166 110 ; 
       147 153 110 ; 
       148 203 110 ; 
       149 137 110 ; 
       150 130 110 ; 
       151 145 110 ; 
       152 300 112 ; 
       152 300 112 2 ; 
       152 300 124 ; 
       152 300 124 2 ; 
       153 190 110 ; 
       154 158 110 ; 
       155 158 110 ; 
       156 159 110 ; 
       157 159 110 ; 
       158 160 110 ; 
       159 160 110 ; 
       160 153 110 ; 
       161 162 110 ; 
       162 153 110 ; 
       163 153 110 ; 
       164 153 110 ; 
       165 153 110 ; 
       166 153 110 ; 
       167 162 110 ; 
       168 189 110 ; 
       169 189 110 ; 
       170 128 110 ; 
       171 173 110 ; 
       172 173 110 ; 
       173 161 110 ; 
       174 161 110 ; 
       175 174 110 ; 
       176 134 110 ; 
       177 176 110 ; 
       178 134 110 ; 
       179 178 110 ; 
       180 187 110 ; 
       181 174 110 ; 
       182 140 110 ; 
       183 140 110 ; 
       184 179 110 ; 
       185 177 110 ; 
       186 187 110 ; 
       187 167 110 ; 
       188 162 110 ; 
       189 167 110 ; 
       190 152 110 ; 
       191 164 110 ; 
       192 164 110 ; 
       193 164 110 ; 
       194 164 110 ; 
       195 145 110 ; 
       196 163 110 ; 
       197 163 110 ; 
       198 163 110 ; 
       199 163 110 ; 
       200 203 110 ; 
       201 203 110 ; 
       202 203 110 ; 
       203 145 110 ; 
       204 165 110 ; 
       205 165 110 ; 
       206 165 110 ; 
       207 165 110 ; 
       208 165 110 ; 
       209 165 110 ; 
       210 155 110 ; 
       211 154 110 ; 
       212 157 110 ; 
       213 156 110 ; 
       214 232 110 ; 
       215 247 110 ; 
       216 239 110 ; 
       217 285 110 ; 
       218 286 110 ; 
       219 233 110 ; 
       220 219 110 ; 
       221 220 110 ; 
       222 220 110 ; 
       223 233 110 ; 
       224 223 110 ; 
       225 224 110 ; 
       226 224 110 ; 
       227 263 110 ; 
       228 266 110 ; 
       229 267 110 ; 
       230 271 110 ; 
       231 247 110 ; 
       232 239 110 ; 
       233 250 110 ; 
       234 286 110 ; 
       235 220 110 ; 
       236 224 110 ; 
       237 233 110 ; 
       238 304 112 ; 
       238 304 112 2 ; 
       239 276 110 ; 
       240 245 110 ; 
       241 244 110 ; 
       242 245 110 ; 
       243 244 110 ; 
       245 246 110 ; 
       244 246 110 ; 
       246 239 110 ; 
       247 239 110 ; 
       248 239 110 ; 
       249 239 110 ; 
       250 239 110 ; 
       251 250 110 ; 
       252 250 110 ; 
       253 239 110 ; 
       254 214 110 ; 
       255 267 110 ; 
       256 267 110 ; 
       257 271 110 ; 
       258 271 110 ; 
       259 216 110 ; 
       260 216 110 ; 
       261 218 110 ; 
       262 261 110 ; 
       263 262 110 ; 
       264 218 110 ; 
       265 264 110 ; 
       266 265 110 ; 
       267 251 110 ; 
       268 251 110 ; 
       269 268 110 ; 
       270 268 110 ; 
       271 252 110 ; 
       272 252 110 ; 
       273 272 110 ; 
       274 272 110 ; 
       275 250 110 ; 
       276 238 110 ; 
       277 248 110 ; 
       278 248 110 ; 
       279 248 110 ; 
       280 248 110 ; 
       281 249 110 ; 
       282 249 110 ; 
       283 249 110 ; 
       284 249 110 ; 
       285 233 110 ; 
       286 233 110 ; 
       287 286 110 ; 
       288 286 110 ; 
       289 286 110 ; 
       290 253 110 ; 
       291 253 110 ; 
       292 253 110 ; 
       293 253 110 ; 
       294 253 110 ; 
       295 253 110 ; 
       296 240 110 ; 
       297 242 110 ; 
       298 241 110 ; 
       299 243 110 ; 
       301 303 110 ; 
       302 303 110 ; 
       303 152 111 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       122 0 300 ; 
       123 0 300 ; 
       124 0 300 ; 
       125 1 300 ; 
       126 1 300 ; 
       127 1 300 ; 
       204 0 300 ; 
       205 0 300 ; 
       206 0 300 ; 
       207 1 300 ; 
       208 1 300 ; 
       209 1 300 ; 
       290 0 300 ; 
       291 0 300 ; 
       292 0 300 ; 
       293 1 300 ; 
       294 1 300 ; 
       295 1 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       1 0 15000 ; 
       152 1 15000 ; 
       238 2 15000 ; 
       303 3 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER MODELS 
       0 301 1111 ; 
       0 302 1116 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS CHAPTER MODELS 
       0 127 551 1 ; 
       0 126 551 1 ; 
       0 125 551 1 ; 
       0 124 551 1 ; 
       0 123 551 1 ; 
       0 122 551 1 ; 
       0 292 551 1 ; 
       0 295 551 1 ; 
       0 294 551 1 ; 
       0 291 551 1 ; 
       0 293 551 1 ; 
       0 290 551 1 ; 
       0 206 551 1 ; 
       0 209 551 1 ; 
       0 208 551 1 ; 
       0 205 551 1 ; 
       0 207 551 1 ; 
       0 204 551 1 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM -3.145763 -1.048588 0 USR MPRFLG 0 ; 
       1 SCHEM -3.145762 -2.60238 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM -36.99583 -166.7793 0 MPRFLG 0 ; 
       1 SCHEM -18.24583 -8.410521 0 USR SRT 1 1 1 0 0 0 3636.743 -103.284 288.5134 MPRFLG 0 ; 
       2 SCHEM -39.49583 -166.7793 0 MPRFLG 0 ; 
       3 SCHEM -86.99583 -168.7793 0 MPRFLG 0 ; 
       4 SCHEM 50.50417 -168.7793 0 MPRFLG 0 ; 
       5 SCHEM 5.504166 -168.7793 0 MPRFLG 0 ; 
       6 SCHEM -86.99583 -170.7793 0 MPRFLG 0 ; 
       7 SCHEM -84.49583 -170.7793 0 MPRFLG 0 ; 
       8 SCHEM -86.99583 -166.7793 0 MPRFLG 0 ; 
       9 SCHEM -79.49583 -166.7793 0 MPRFLG 0 ; 
       10 SCHEM -79.49583 -168.7793 0 MPRFLG 0 ; 
       11 SCHEM -79.49583 -170.7793 0 MPRFLG 0 ; 
       12 SCHEM -76.99583 -170.7793 0 MPRFLG 0 ; 
       13 SCHEM -44.49583 -166.7793 0 MPRFLG 0 ; 
       14 SCHEM -79.49583 -164.7793 0 DISPLAY 1 2 MPRFLG 0 ; 
       15 SCHEM -68.24583 -166.7793 0 MPRFLG 0 ; 
       16 SCHEM -81.99583 -170.7793 0 MPRFLG 0 ; 
       17 SCHEM -41.99583 -166.7793 0 MPRFLG 0 ; 
       18 SCHEM -36.99583 -164.7793 0 MPRFLG 0 ; 
       19 SCHEM -46.99583 -166.7793 0 MPRFLG 0 ; 
       20 SCHEM -89.49583 -170.7793 0 MPRFLG 0 ; 
       21 SCHEM -91.99583 -166.7793 0 MPRFLG 0 ; 
       22 SCHEM -64.49583 -168.7793 0 MPRFLG 0 ; 
       23 SCHEM -10.74583 -162.7793 0 MPRFLG 0 ; 
       24 SCHEM 65.50417 -164.7793 0 WIRECOL 4 7 MPRFLG 0 ; 
       25 SCHEM 68.00417 -164.7793 0 WIRECOL 4 7 MPRFLG 0 ; 
       26 SCHEM 70.50417 -164.7793 0 WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 73.00417 -164.7793 0 WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM -71.99583 -168.7793 0 MPRFLG 0 ; 
       29 SCHEM 56.75417 -164.7793 0 MPRFLG 0 ; 
       30 SCHEM -105.7458 -162.7793 0 MPRFLG 0 ; 
       31 SCHEM -30.74583 -164.7793 0 MPRFLG 0 ; 
       32 SCHEM 16.75417 -164.7793 0 MPRFLG 0 ; 
       33 SCHEM -40.74583 -164.7793 0 MPRFLG 0 ; 
       34 SCHEM 61.75417 -164.7793 0 MPRFLG 0 ; 
       35 SCHEM 3.004166 -168.7793 0 MPRFLG 0 ; 
       36 SCHEM 60.50417 -168.7793 0 MPRFLG 0 ; 
       37 SCHEM 63.00417 -168.7793 0 MPRFLG 0 ; 
       38 SCHEM -36.99583 -168.7793 0 MPRFLG 0 ; 
       39 SCHEM 58.00417 -168.7793 0 MPRFLG 0 ; 
       40 SCHEM 55.50417 -168.7793 0 MPRFLG 0 ; 
       41 SCHEM 53.00417 -168.7793 0 MPRFLG 0 ; 
       42 SCHEM 8.004166 -166.7793 0 MPRFLG 0 ; 
       43 SCHEM 56.75417 -166.7793 0 MPRFLG 0 ; 
       44 SCHEM -56.99583 -166.7793 0 MPRFLG 0 ; 
       45 SCHEM -56.99583 -168.7793 0 MPRFLG 0 ; 
       46 SCHEM -96.99583 -168.7793 0 MPRFLG 0 ; 
       47 SCHEM -94.49583 -168.7793 0 MPRFLG 0 ; 
       48 SCHEM -24.49583 -170.7793 0 MPRFLG 0 ; 
       49 SCHEM -21.99583 -170.7793 0 MPRFLG 0 ; 
       50 SCHEM -19.49583 -170.7793 0 MPRFLG 0 ; 
       51 SCHEM -16.99583 -170.7793 0 MPRFLG 0 ; 
       52 SCHEM -14.49583 -170.7793 0 MPRFLG 0 ; 
       53 SCHEM -11.99583 -170.7793 0 MPRFLG 0 ; 
       54 SCHEM -9.495834 -170.7793 0 MPRFLG 0 ; 
       55 SCHEM -6.995834 -170.7793 0 MPRFLG 0 ; 
       56 SCHEM -4.495834 -170.7793 0 MPRFLG 0 ; 
       57 SCHEM -1.995834 -170.7793 0 MPRFLG 0 ; 
       58 SCHEM 0.5041656 -170.7793 0 MPRFLG 0 ; 
       59 SCHEM 23.00417 -168.7793 0 MPRFLG 0 ; 
       60 SCHEM 23.00417 -170.7793 0 MPRFLG 0 ; 
       61 SCHEM 25.50417 -168.7793 0 MPRFLG 0 ; 
       62 SCHEM 25.50417 -170.7793 0 MPRFLG 0 ; 
       63 SCHEM 28.00417 -168.7793 0 MPRFLG 0 ; 
       64 SCHEM 28.00417 -170.7793 0 MPRFLG 0 ; 
       65 SCHEM 30.50417 -168.7793 0 MPRFLG 0 ; 
       66 SCHEM 30.50417 -170.7793 0 MPRFLG 0 ; 
       67 SCHEM 33.00417 -168.7793 0 MPRFLG 0 ; 
       68 SCHEM 33.00417 -170.7793 0 MPRFLG 0 ; 
       69 SCHEM 35.50417 -168.7793 0 MPRFLG 0 ; 
       70 SCHEM 35.50417 -170.7793 0 MPRFLG 0 ; 
       71 SCHEM 38.00417 -168.7793 0 MPRFLG 0 ; 
       72 SCHEM 38.00417 -170.7793 0 MPRFLG 0 ; 
       73 SCHEM 40.50417 -168.7793 0 MPRFLG 0 ; 
       74 SCHEM 40.50417 -170.7793 0 MPRFLG 0 ; 
       75 SCHEM 43.00417 -168.7793 0 MPRFLG 0 ; 
       76 SCHEM 43.00417 -170.7793 0 MPRFLG 0 ; 
       77 SCHEM 45.50417 -168.7793 0 MPRFLG 0 ; 
       78 SCHEM 45.50417 -170.7793 0 MPRFLG 0 ; 
       79 SCHEM 48.00417 -168.7793 0 MPRFLG 0 ; 
       80 SCHEM 48.00417 -170.7793 0 MPRFLG 0 ; 
       81 SCHEM -59.49583 -166.7793 0 MPRFLG 0 ; 
       82 SCHEM -59.49583 -168.7793 0 MPRFLG 0 ; 
       83 SCHEM 8.004166 -168.7793 0 MPRFLG 0 ; 
       84 SCHEM -69.49583 -168.7793 0 MPRFLG 0 ; 
       85 SCHEM -66.99583 -168.7793 0 MPRFLG 0 ; 
       86 SCHEM -71.99583 -170.7793 0 MPRFLG 0 ; 
       87 SCHEM -61.99583 -168.7793 0 MPRFLG 0 ; 
       88 SCHEM 10.50417 -168.7793 0 MPRFLG 0 ; 
       89 SCHEM 10.50417 -166.7793 0 MPRFLG 0 ; 
       90 SCHEM -14.49583 -168.7793 0 MPRFLG 0 ; 
       91 SCHEM -11.99583 -168.7793 0 MPRFLG 0 ; 
       92 SCHEM -9.495834 -168.7793 0 MPRFLG 0 ; 
       93 SCHEM -6.995834 -168.7793 0 MPRFLG 0 ; 
       94 SCHEM -4.495834 -168.7793 0 MPRFLG 0 ; 
       95 SCHEM -1.995834 -168.7793 0 MPRFLG 0 ; 
       96 SCHEM 0.5041656 -168.7793 0 MPRFLG 0 ; 
       97 SCHEM -16.99583 -168.7793 0 MPRFLG 0 ; 
       98 SCHEM -19.49583 -168.7793 0 MPRFLG 0 ; 
       99 SCHEM -21.99583 -168.7793 0 MPRFLG 0 ; 
       100 SCHEM -24.49583 -168.7793 0 MPRFLG 0 ; 
       101 SCHEM 9.254166 -164.7793 0 MPRFLG 0 ; 
       102 SCHEM 61.75417 -166.7793 0 MPRFLG 0 ; 
       103 SCHEM -18.24583 -160.7793 0 USR MPRFLG 0 ; 
       104 SCHEM -74.49583 -168.7793 0 MPRFLG 0 ; 
       105 SCHEM -34.49583 -166.7793 0 MPRFLG 0 ; 
       106 SCHEM -31.99583 -166.7793 0 MPRFLG 0 ; 
       107 SCHEM -29.49583 -166.7793 0 MPRFLG 0 ; 
       108 SCHEM -26.99583 -166.7793 0 MPRFLG 0 ; 
       109 SCHEM 20.50417 -166.7793 0 MPRFLG 0 ; 
       110 SCHEM 18.00417 -166.7793 0 MPRFLG 0 ; 
       111 SCHEM 15.50417 -166.7793 0 MPRFLG 0 ; 
       112 SCHEM 13.00417 -166.7793 0 MPRFLG 0 ; 
       113 SCHEM 38.00417 -166.7793 0 MPRFLG 0 ; 
       114 SCHEM 38.00417 -164.7793 0 MPRFLG 0 ; 
       115 SCHEM -9.495834 -164.7793 0 MPRFLG 0 ; 
       116 SCHEM -9.495834 -166.7793 0 MPRFLG 0 ; 
       117 SCHEM -95.74583 -166.7793 0 MPRFLG 0 ; 
       118 SCHEM -54.49583 -166.7793 0 MPRFLG 0 ; 
       119 SCHEM -51.99583 -166.7793 0 MPRFLG 0 ; 
       120 SCHEM -49.49583 -166.7793 0 MPRFLG 0 ; 
       121 SCHEM -51.99583 -164.7793 0 MPRFLG 0 ; 
       122 SCHEM -111.9958 -164.7793 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       123 SCHEM -109.4958 -164.7793 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       124 SCHEM -106.9958 -164.7793 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       125 SCHEM -99.49583 -164.7793 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       126 SCHEM -101.9958 -164.7793 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       127 SCHEM -104.4958 -164.7793 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       128 SCHEM -3.273239 -120.2959 0 MPRFLG 0 ; 
       129 SCHEM -68.27324 -120.2959 0 MPRFLG 0 ; 
       130 SCHEM -10.77324 -124.2959 0 MPRFLG 0 ; 
       131 SCHEM -10.77324 -126.2959 0 MPRFLG 0 ; 
       132 SCHEM -8.273239 -126.2959 0 MPRFLG 0 ; 
       133 SCHEM -10.77324 -122.2959 0 MPRFLG 0 ; 
       134 SCHEM -24.52324 -124.2959 0 MPRFLG 0 ; 
       135 SCHEM -38.27324 -124.2959 0 MPRFLG 0 ; 
       136 SCHEM -18.27324 -122.2959 0 MPRFLG 0 ; 
       137 SCHEM -18.27324 -124.2959 0 MPRFLG 0 ; 
       138 SCHEM -18.27324 -126.2959 0 MPRFLG 0 ; 
       139 SCHEM -15.77324 -126.2959 0 MPRFLG 0 ; 
       140 SCHEM 20.47676 -118.2959 0 MPRFLG 0 ; 
       141 SCHEM -48.27324 -124.2959 0 WIRECOL 4 7 MPRFLG 0 ; 
       142 SCHEM -55.77324 -124.2959 0 WIRECOL 4 7 MPRFLG 0 ; 
       143 SCHEM -25.77324 -132.2959 0 WIRECOL 4 7 MPRFLG 0 ; 
       144 SCHEM -23.27324 -132.2959 0 WIRECOL 4 7 MPRFLG 0 ; 
       145 SCHEM -24.52324 -120.2959 0 MPRFLG 0 ; 
       146 SCHEM -70.77324 -120.2959 0 MPRFLG 0 ; 
       147 SCHEM -3.273239 -118.2959 0 MPRFLG 0 ; 
       148 SCHEM -28.27324 -124.2959 0 MPRFLG 0 ; 
       149 SCHEM -20.77324 -126.2959 0 MPRFLG 0 ; 
       150 SCHEM -13.27324 -126.2959 0 MPRFLG 0 ; 
       151 SCHEM -40.77324 -122.2959 0 MPRFLG 0 ; 
       152 SCHEM -5.552814 -6.55296 0 USR SRT 1 1 1 0 0 0 3602.976 -92.02856 278.6617 MPRFLG 0 ; 
       153 SCHEM -25.77324 -116.2959 0 MPRFLG 0 ; 
       154 SCHEM 29.22676 -122.2959 0 MPRFLG 0 ; 
       155 SCHEM 31.72676 -122.2959 0 MPRFLG 0 ; 
       156 SCHEM 26.72676 -122.2959 0 MPRFLG 0 ; 
       157 SCHEM 24.22676 -122.2959 0 MPRFLG 0 ; 
       158 SCHEM 30.47676 -120.2959 0 MPRFLG 0 ; 
       159 SCHEM 25.47676 -120.2959 0 MPRFLG 0 ; 
       160 SCHEM 27.97676 -118.2959 0 MPRFLG 0 ; 
       161 SCHEM -60.77324 -120.2959 0 MPRFLG 0 ; 
       162 SCHEM -35.77324 -118.2959 0 MPRFLG 0 ; 
       163 SCHEM 12.97676 -118.2959 0 MPRFLG 0 ; 
       164 SCHEM 2.976761 -118.2959 0 MPRFLG 0 ; 
       165 SCHEM -79.52324 -118.2959 0 MPRFLG 0 ; 
       166 SCHEM -69.52324 -118.2959 0 MPRFLG 0 ; 
       167 SCHEM -48.27324 -120.2959 0 MPRFLG 0 ; 
       168 SCHEM -53.27324 -124.2959 0 MPRFLG 0 ; 
       169 SCHEM -50.77324 -124.2959 0 MPRFLG 0 ; 
       170 SCHEM -3.273239 -122.2959 0 MPRFLG 0 ; 
       171 SCHEM -58.27324 -124.2959 0 MPRFLG 0 ; 
       172 SCHEM -60.77324 -124.2959 0 MPRFLG 0 ; 
       173 SCHEM -58.27324 -122.2959 0 MPRFLG 0 ; 
       174 SCHEM -64.52324 -122.2959 0 MPRFLG 0 ; 
       175 SCHEM -63.27324 -124.2959 0 MPRFLG 0 ; 
       176 SCHEM -25.77324 -126.2959 0 MPRFLG 0 ; 
       177 SCHEM -25.77324 -128.2959 0 MPRFLG 0 ; 
       178 SCHEM -23.27324 -126.2959 0 MPRFLG 0 ; 
       179 SCHEM -23.27324 -128.2959 0 MPRFLG 0 ; 
       180 SCHEM -43.27324 -124.2959 0 MPRFLG 0 ; 
       181 SCHEM -65.77324 -124.2959 0 MPRFLG 0 ; 
       182 SCHEM 19.22676 -120.2959 0 MPRFLG 0 ; 
       183 SCHEM 21.72676 -120.2959 0 MPRFLG 0 ; 
       184 SCHEM -23.27324 -130.2959 0 MPRFLG 0 ; 
       185 SCHEM -25.77324 -130.2959 0 MPRFLG 0 ; 
       186 SCHEM -45.77324 -124.2959 0 MPRFLG 0 ; 
       187 SCHEM -44.52324 -122.2959 0 MPRFLG 0 ; 
       188 SCHEM -5.773239 -120.2959 0 MPRFLG 0 ; 
       189 SCHEM -50.77324 -122.2959 0 MPRFLG 0 ; 
       190 SCHEM -25.77324 -114.2959 0 USR MPRFLG 0 ; 
       191 SCHEM -0.7732391 -120.2959 0 MPRFLG 0 ; 
       192 SCHEM 1.726761 -120.2959 0 MPRFLG 0 ; 
       193 SCHEM 4.226761 -120.2959 0 MPRFLG 0 ; 
       194 SCHEM 6.726761 -120.2959 0 MPRFLG 0 ; 
       195 SCHEM -38.27324 -122.2959 0 MPRFLG 0 ; 
       196 SCHEM 16.72676 -120.2959 0 MPRFLG 0 ; 
       197 SCHEM 14.22676 -120.2959 0 MPRFLG 0 ; 
       198 SCHEM 11.72676 -120.2959 0 MPRFLG 0 ; 
       199 SCHEM 9.226761 -120.2959 0 MPRFLG 0 ; 
       200 SCHEM -35.77324 -124.2959 0 MPRFLG 0 ; 
       201 SCHEM -33.27324 -124.2959 0 MPRFLG 0 ; 
       202 SCHEM -30.77324 -124.2959 0 MPRFLG 0 ; 
       203 SCHEM -29.52324 -122.2959 0 MPRFLG 0 ; 
       204 SCHEM -80.77324 -120.2959 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       205 SCHEM -83.27324 -120.2959 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       206 SCHEM -85.77324 -120.2959 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       207 SCHEM -78.27324 -120.2959 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       208 SCHEM -75.77324 -120.2959 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       209 SCHEM -73.27324 -120.2959 0 WIRECOL 4 7 MPRFLG 0 ; 
       210 SCHEM 31.72676 -124.2959 0 MPRFLG 0 ; 
       211 SCHEM 29.22676 -124.2959 0 MPRFLG 0 ; 
       212 SCHEM 24.22676 -124.2959 0 MPRFLG 0 ; 
       213 SCHEM 26.72676 -124.2959 0 MPRFLG 0 ; 
       214 SCHEM -5.537468 -140.9569 0 MPRFLG 0 ; 
       215 SCHEM -70.53747 -140.9569 0 MPRFLG 0 ; 
       216 SCHEM 18.21253 -138.9569 0 MPRFLG 0 ; 
       217 SCHEM -40.53747 -144.9569 0 MPRFLG 0 ; 
       218 SCHEM -26.78747 -144.9569 0 MPRFLG 0 ; 
       219 SCHEM -13.03747 -142.9569 0 MPRFLG 0 ; 
       220 SCHEM -13.03747 -144.9569 0 MPRFLG 0 ; 
       221 SCHEM -13.03747 -146.9569 0 MPRFLG 0 ; 
       222 SCHEM -10.53747 -146.9569 0 MPRFLG 0 ; 
       223 SCHEM -20.53747 -142.9569 0 MPRFLG 0 ; 
       224 SCHEM -20.53747 -144.9569 0 MPRFLG 0 ; 
       225 SCHEM -20.53747 -146.9569 0 MPRFLG 0 ; 
       226 SCHEM -18.03747 -146.9569 0 MPRFLG 0 ; 
       227 SCHEM -25.53747 -152.9569 0 WIRECOL 4 7 MPRFLG 0 ; 
       228 SCHEM -28.03747 -152.9569 0 WIRECOL 4 7 MPRFLG 0 ; 
       229 SCHEM -58.03747 -144.9569 0 WIRECOL 4 7 MPRFLG 0 ; 
       230 SCHEM -50.53747 -144.9569 0 WIRECOL 4 7 MPRFLG 0 ; 
       231 SCHEM -73.03747 -140.9569 0 MPRFLG 0 ; 
       232 SCHEM -5.537468 -138.9569 0 MPRFLG 0 ; 
       233 SCHEM -26.78747 -140.9569 0 MPRFLG 0 ; 
       234 SCHEM -30.53747 -144.9569 0 MPRFLG 0 ; 
       235 SCHEM -15.53747 -146.9569 0 MPRFLG 0 ; 
       236 SCHEM -23.03747 -146.9569 0 MPRFLG 0 ; 
       237 SCHEM -43.03747 -142.9569 0 MPRFLG 0 ; 
       238 SCHEM -12.96383 -6.485353 0 USR SRT 1 1 1 0 0 0 3597.131 -85.81752 263.395 MPRFLG 0 ; 
       239 SCHEM -29.28747 -136.9569 0 MPRFLG 0 ; 
       240 SCHEM 29.46253 -142.9569 0 MPRFLG 0 ; 
       241 SCHEM 24.46253 -142.9569 0 MPRFLG 0 ; 
       242 SCHEM 26.96253 -142.9569 0 MPRFLG 0 ; 
       243 SCHEM 21.96253 -142.9569 0 MPRFLG 0 ; 
       245 SCHEM 28.21253 -140.9569 0 MPRFLG 0 ; 
       244 SCHEM 23.21253 -140.9569 0 MPRFLG 0 ; 
       246 SCHEM 25.71253 -138.9569 0 MPRFLG 0 ; 
       247 SCHEM -71.78747 -138.9569 0 MPRFLG 0 ; 
       248 SCHEM 0.712532 -138.9569 0 MPRFLG 0 ; 
       249 SCHEM 10.71253 -138.9569 0 MPRFLG 0 ; 
       250 SCHEM -38.03747 -138.9569 0 MPRFLG 0 ; 
       251 SCHEM -63.03747 -140.9569 0 MPRFLG 0 ; 
       252 SCHEM -50.53747 -140.9569 0 MPRFLG 0 ; 
       253 SCHEM -81.78747 -138.9569 0 MPRFLG 0 ; 
       254 SCHEM -5.537468 -142.9569 0 MPRFLG 0 ; 
       255 SCHEM -60.53747 -144.9569 0 MPRFLG 0 ; 
       256 SCHEM -63.03747 -144.9569 0 MPRFLG 0 ; 
       257 SCHEM -53.03747 -144.9569 0 MPRFLG 0 ; 
       258 SCHEM -55.53747 -144.9569 0 MPRFLG 0 ; 
       259 SCHEM 19.46253 -140.9569 0 MPRFLG 0 ; 
       260 SCHEM 16.96253 -140.9569 0 MPRFLG 0 ; 
       261 SCHEM -25.53747 -146.9569 0 MPRFLG 0 ; 
       262 SCHEM -25.53747 -148.9569 0 MPRFLG 0 ; 
       263 SCHEM -25.53747 -150.9569 0 MPRFLG 0 ; 
       264 SCHEM -28.03747 -146.9569 0 MPRFLG 0 ; 
       265 SCHEM -28.03747 -148.9569 0 MPRFLG 0 ; 
       266 SCHEM -28.03747 -150.9569 0 MPRFLG 0 ; 
       267 SCHEM -60.53747 -142.9569 0 MPRFLG 0 ; 
       268 SCHEM -66.78747 -142.9569 0 MPRFLG 0 ; 
       269 SCHEM -65.53747 -144.9569 0 MPRFLG 0 ; 
       270 SCHEM -68.03747 -144.9569 0 MPRFLG 0 ; 
       271 SCHEM -53.03747 -142.9569 0 MPRFLG 0 ; 
       272 SCHEM -46.78747 -142.9569 0 MPRFLG 0 ; 
       273 SCHEM -48.03747 -144.9569 0 MPRFLG 0 ; 
       274 SCHEM -45.53747 -144.9569 0 MPRFLG 0 ; 
       275 SCHEM -8.037468 -140.9569 0 MPRFLG 0 ; 
       276 SCHEM -28.03747 -134.9569 0 USR MPRFLG 0 ; 
       277 SCHEM 4.462532 -140.9569 0 MPRFLG 0 ; 
       278 SCHEM 1.962532 -140.9569 0 MPRFLG 0 ; 
       279 SCHEM -0.537468 -140.9569 0 MPRFLG 0 ; 
       280 SCHEM -3.037468 -140.9569 0 MPRFLG 0 ; 
       281 SCHEM 14.46253 -140.9569 0 MPRFLG 0 ; 
       282 SCHEM 11.96253 -140.9569 0 MPRFLG 0 ; 
       283 SCHEM 9.462532 -140.9569 0 MPRFLG 0 ; 
       284 SCHEM 6.962532 -140.9569 0 MPRFLG 0 ; 
       285 SCHEM -40.53747 -142.9569 0 MPRFLG 0 ; 
       286 SCHEM -31.78747 -142.9569 0 MPRFLG 0 ; 
       287 SCHEM -33.03747 -144.9569 0 MPRFLG 0 ; 
       288 SCHEM -35.53747 -144.9569 0 MPRFLG 0 ; 
       289 SCHEM -38.03747 -144.9569 0 MPRFLG 0 ; 
       290 SCHEM -83.03747 -140.9569 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       291 SCHEM -85.53747 -140.9569 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       292 SCHEM -88.03747 -140.9569 0 WIRECOL 2 7 DISPLAY 0 0 MPRFLG 0 ; 
       293 SCHEM -80.53747 -140.9569 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       294 SCHEM -78.03747 -140.9569 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       295 SCHEM -75.53747 -140.9569 0 WIRECOL 4 7 DISPLAY 0 0 MPRFLG 0 ; 
       296 SCHEM 29.46253 -144.9569 0 MPRFLG 0 ; 
       297 SCHEM 26.96253 -144.9569 0 MPRFLG 0 ; 
       298 SCHEM 24.46253 -144.9569 0 MPRFLG 0 ; 
       299 SCHEM 21.96253 -144.9569 0 MPRFLG 0 ; 
       300 SCHEM -6.344467 -3.892068 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       301 SCHEM -8.225212 -1.858974 0 USR MPRFLG 0 ; 
       302 SCHEM -12.47005 -1.808634 0 USR MPRFLG 0 ; 
       303 SCHEM -8.225212 0.1410266 0 USR SRT 1 1 1 0 0 0 3602.976 -92.02856 278.6617 MPRFLG 0 ; 
       304 SCHEM -1.57609 -3.732685 0 USR SRT 1 1 1 0 0 0 -5.845783 6.211048 -15.26674 MPRFLG 0 ; 
       305 SCHEM -16.38638 -4.531955 0 USR SRT 1 1 1 0 0 0 33.76643 -11.25548 9.851652 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM -104.8399 -149.2579 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM -102.1571 -149.3496 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 168.326 0.1410266 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 168.326 0.1410266 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 168.326 0.1410266 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 168.326 0.1670423 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 100 450 450 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
