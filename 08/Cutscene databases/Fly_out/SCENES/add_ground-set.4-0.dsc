SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 4     
       Bomber2-Bom_root.35-0 ; 
       Fig_root-Fig_Root.46-0 ; 
       Fig_root3-Fig_Root.34-0 ; 
       Platform-Root.56-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       tur_cones-cam_int1.51-0 ROOT ; 
       tur_cones-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS NBELEM 1     
       set-inf_light1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 3     
       lighting_ships-copper1.1-0 ; 
       lighting_ships-mat423.1-0 ; 
       set-ground.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 10     
       Bomber2-Bom_root.31-0 ROOT ; 
       Fig_root-Fig_Root.42-0 ROOT ; 
       Fig_root3-Fig_Root.25-0 ROOT ; 
       lighting_ships-Fighter_cam_Path_1.1-0 ROOT ; 
       Platform-Cam.1-0 ; 
       Platform-Int.1-0 ; 
       Platform-Root.53-0 ROOT ; 
       set-grid5.3-0 ROOT ; 
       Set1-Fig2_Path.12-0 ROOT ; 
       Set2-Fighter_cam_Path.13-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS NBELEM 2     
       base-DGlow1.16-0 ; 
       station-hangarglow2.11-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 2     
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/MoonBump_Ground ; 
       I:/Cutscene/Production/Softimage/Fly_out/PICTURES/MoonMap_ground ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       add_ground-set.4-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE2D_SHADERS NBELEM 1     
       set-Terrain1.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 3     
       set-t2d1.1-0 ; 
       set-t2d2.2-0 ; 
       set-t2d3.2-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D NBELEM 1     
       set-rock1.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 9 112 ; 
       0 9 112 2 ; 
       0 9 124 ; 
       0 9 124 2 ; 
       1 3 112 ; 
       1 3 112 2 ; 
       1 3 124 ; 
       1 3 124 2 ; 
       2 8 112 ; 
       2 8 112 2 ; 
       4 6 110 ; 
       5 6 110 ; 
       6 1 111 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       7 2 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       7 0 400 ; 
       7 1 400 ; 
       7 2 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES3D 
       7 0 500 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       0 0 15000 ; 
       1 1 15000 ; 
       2 2 15000 ; 
       6 3 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER MODELS 
       0 4 1111 ; 
       0 5 1116 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D CHAPTER TEXTURE2D_SHADERS 
       1 0 550 ; 
    EndOfCHAPTER 

    CHAPTER SCENES CHAPTER OUTPUT_SHADERS 
       0 0 550 ; 
       0 1 550 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM -3.145763 -1.048588 0 USR MPRFLG 0 ; 
       1 SCHEM -3.145762 -2.60238 0 USR MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER LIGHTS 
       0 SCHEM 16.84658 0.7657303 0 WIRECOL 7 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       0 SCHEM -18.24583 -8.410521 0 USR SRT 1 1 1 0 0 0 54.5469 -103.284 288.5134 MPRFLG 0 ; 
       1 SCHEM -5.552814 -6.55296 0 USR SRT 1 1 1 0 0 0 20.78047 -92.02856 278.6617 MPRFLG 0 ; 
       2 SCHEM -12.96383 -6.485353 0 USR SRT 1 1 1 0 0 0 -56.514 -85.81752 263.395 MPRFLG 0 ; 
       3 SCHEM -6.344467 -3.892068 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM -8.225212 -1.858974 0 USR MPRFLG 0 ; 
       5 SCHEM -12.47005 -1.808634 0 USR MPRFLG 0 ; 
       6 SCHEM -8.225212 0.1410266 0 USR SRT 1 1 1 0 0 0 20.78047 -92.02856 278.6617 MPRFLG 0 ; 
       8 SCHEM -1.57609 -3.732685 0 USR SRT 1 1 1 0 0 0 -5.845783 6.211048 -15.26674 MPRFLG 0 ; 
       9 SCHEM -16.38638 -4.531955 0 USR SRT 1 1 1 0 0 0 33.76643 -11.25548 9.851652 MPRFLG 0 ; 
       7 SCHEM 4.346576 0.7657303 0 USR DISPLAY 1 2 SRT 1 0.2352087 1 0 0 0 0 -494.6454 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       2 SCHEM 9.346576 0.7657303 0 WIRECOL 1 7 MPRFLG 0 ; 
       0 SCHEM 102.7232 -31.1327 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 101.5747 -33.08318 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 11.84658 0.7657303 0 WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 14.34658 0.7657303 0 WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 19.34658 0.7657303 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES3D 
       0 SCHEM 21.84658 0.7657303 0 WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 168.326 0.1410266 0 WIRECOL 9 7 MPRFLG 0 ; 
       1 SCHEM 168.326 0.1410266 0 WIRECOL 9 7 MPRFLG 0 ; 
       2 SCHEM 168.326 0.1410266 0 WIRECOL 9 7 MPRFLG 0 ; 
       3 SCHEM 168.326 0.1670423 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURE2D_SHADERS 
       0 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    CHAPTER OUTPUT_SHADERS 
       0 SCHEM 0 0 0 ; 
       1 SCHEM 0 0 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 100 450 100 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
