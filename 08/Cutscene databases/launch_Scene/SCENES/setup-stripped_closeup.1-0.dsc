SDSC3.71
ELEMENTS 
    CHAPTER ANIMATION NBELEM 1     
       stripped_closeup-extru58.1-0 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS NBELEM 2     
       fixer-cam_int1.5-0 ROOT ; 
       fixer-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 16     
       Fighter-Base_Green1_1.1-0 ; 
       Fighter-Canopy1_1.1-0 ; 
       Fighter-Dark1.1-0 ; 
       Fighter-Light_Glass.1-0 ; 
       Fighter-mat2.1-0 ; 
       Fighter-mat3.1-0 ; 
       Fighter-mat4.1-0 ; 
       Fighter-mat5.1-0 ; 
       Fighter-mat6.1-0 ; 
       Fighter-METALS-chrome003.1-0.1-0 ; 
       Fighter-METALS-GRAPHITE.1-1.1-0 ; 
       Fighter-METALS-GRAPHITE.1-2.1-0 ; 
       Fighter-METALS-GRAPHITE.1-3_1.1-0 ; 
       Fighter-METALS-GRAPHITE.1-4_1.1-0 ; 
       Fighter-METALS-GRAPHITE.1-5_1.1-0 ; 
       Fighter-WeatherStrip1_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 70     
       stripped_closeup-BIG_CANOPY_1.1-0 ; 
       stripped_closeup-bottom20.1-0 ; 
       stripped_closeup-cube59.1-0 ; 
       stripped_closeup-cube60_1.1-0 ; 
       stripped_closeup-cube61_1.1-0 ; 
       stripped_closeup-cube62_1.1-0 ; 
       stripped_closeup-cube63.1-0 ; 
       stripped_closeup-cube64.1-0 ; 
       stripped_closeup-cube65.1-0 ; 
       stripped_closeup-cube66_1.1-0 ; 
       stripped_closeup-cube67_1.1-0 ; 
       stripped_closeup-cube68.1-0 ; 
       stripped_closeup-cube70.1-0 ; 
       stripped_closeup-cube71.1-0 ; 
       stripped_closeup-cube72.1-0 ; 
       stripped_closeup-cube73.1-0 ; 
       stripped_closeup-cube74.1-0 ; 
       stripped_closeup-cube75.1-0 ; 
       stripped_closeup-cube76.1-0 ; 
       stripped_closeup-cube77.2-0 ; 
       stripped_closeup-cube78.1-0 ; 
       stripped_closeup-cube79.1-0 ; 
       stripped_closeup-cube80.1-0 ; 
       stripped_closeup-cube81.1-0 ; 
       stripped_closeup-cube82.1-0 ; 
       stripped_closeup-cube83.1-0 ; 
       stripped_closeup-engine_holder.1-0 ; 
       stripped_closeup-engine_holder1.1-0 ; 
       stripped_closeup-extru57.1-0 ; 
       stripped_closeup-extru58.1-0 ROOT ; 
       stripped_closeup-extru59.1-0 ; 
       stripped_closeup-FUSELAGE_1.14-0 ; 
       stripped_closeup-GuidedExtrude1_1.1-0 ; 
       stripped_closeup-GuidedExtrude2_1.1-0 ; 
       stripped_closeup-lamp.1-0 ; 
       stripped_closeup-lamp_1.1-0 ; 
       stripped_closeup-lamp_2.1-0 ; 
       stripped_closeup-lamp1.1-0 ; 
       stripped_closeup-light_Area.1-0 ; 
       stripped_closeup-light_area.1-0 ; 
       stripped_closeup-Light_Cover.1-0 ; 
       stripped_closeup-null11.1-0 ; 
       stripped_closeup-null12.7-0 ; 
       stripped_closeup-null13.1-0 ; 
       stripped_closeup-null18.1-0 ROOT ; 
       stripped_closeup-null19.1-0 ROOT ; 
       stripped_closeup-null7.1-0 ; 
       stripped_closeup-null8.1-0 ; 
       stripped_closeup-nurbs25.1-0 ; 
       stripped_closeup-nurbs27.1-0 ; 
       stripped_closeup-nurbs33_1.1-0 ; 
       stripped_closeup-nurbs39.1-0 ; 
       stripped_closeup-nurbs40.1-0 ; 
       stripped_closeup-revol110.1-0 ; 
       stripped_closeup-revol111.1-0 ; 
       stripped_closeup-revol112.1-0 ; 
       stripped_closeup-revol149.1-0 ; 
       stripped_closeup-revol150.1-0 ; 
       stripped_closeup-revol151.1-0 ; 
       stripped_closeup-revol152.1-0 ; 
       stripped_closeup-revol65.1-0 ; 
       stripped_closeup-revol68.1-0 ; 
       stripped_closeup-revol94.1-0 ; 
       stripped_closeup-revol99.1-0 ; 
       stripped_closeup-Root.1-0 ; 
       stripped_closeup-skin27.1-0 ; 
       stripped_closeup-skin28.1-0 ; 
       stripped_closeup-skin29.1-0 ; 
       stripped_closeup-skin30.1-0 ; 
       stripped_closeup-sphere1_1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER PICTURES NBELEM 39     
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/Bars ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/Thrust_bump_lines ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/Thrust_spec_tiles ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/Thrust_tiles ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/acs63 ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/arrow ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/arrow2 ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/backgr ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/black ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/bluecube ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/bom01a ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/console ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/cube ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/cubes ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/dotspic ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/ducttube ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/eject ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/facemask ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/fightertorso ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/fuselage_bumpLines ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/fuselage_tiles ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/granit ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/hazzard ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/helmet_map ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/intake_Soot ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/mainbayouter ; 
       C:/SOFT3D_3.7SP1/3D/bin/rsrc/noIcon ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/reflection ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/spec_tiles ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/specular_grain ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/ss300 ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/thrust_bump ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/thrust_tiles ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/torso ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/triangle_decal ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/triangle_decal2 ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/util_seat ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/utl19 ; 
       //research/root/federation/Shared_Art_Files/Cutscene/Production/launch_bay/launch_Scene/PICTURES/visortop ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       setup-stripped_closeup.1-0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D NBELEM 41     
       Fighter-arrow1.1-0 ; 
       Fighter-arrow2.1-0 ; 
       Fighter-Bars_Base1_1.1-0 ; 
       Fighter-Bars1_1.1-0 ; 
       Fighter-black1.1-0 ; 
       Fighter-black2.1-0 ; 
       Fighter-bump_lines1.1-0 ; 
       Fighter-bump_lines3.1-0 ; 
       Fighter-bump_lines4.1-0 ; 
       Fighter-Cubes1.1-0 ; 
       Fighter-Cubes3.1-0 ; 
       Fighter-Cubes4.1-0 ; 
       Fighter-dots1.1-0 ; 
       Fighter-dots2.1-0 ; 
       Fighter-Fuge_Bump.1-0 ; 
       Fighter-Fuge_Bump1.1-0 ; 
       Fighter-Fuge_Bump2.1-0 ; 
       Fighter-Hazzard1.1-0 ; 
       Fighter-Hazzard2.1-0 ; 
       Fighter-Intake_Soot1.1-0 ; 
       Fighter-nose1.1-0 ; 
       Fighter-ref1.1-0 ; 
       Fighter-reflection1_1.1-0 ; 
       Fighter-Speck_Kill1_1.1-0 ; 
       Fighter-specular_grain1.1-0 ; 
       Fighter-specular_grain5.1-0 ; 
       Fighter-specular_grain6.1-0 ; 
       Fighter-Spec_Killer.1-0 ; 
       Fighter-Spec_Killer2.1-0 ; 
       Fighter-Spec_Killer3.1-0 ; 
       Fighter-spec_tiles1.1-0 ; 
       Fighter-spec_tiles3.1-0 ; 
       Fighter-spec_tiles4.1-0 ; 
       Fighter-t2d1.1-0 ; 
       Fighter-t2d2.1-0 ; 
       Fighter-t2d3.1-0 ; 
       Fighter-Tiles1.1-0 ; 
       Fighter-Tiles3.1-0 ; 
       Fighter-Tiles4.1-0 ; 
       Fighter-triangle1.1-0 ; 
       Fighter-triangle2.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       0 29 110 ; 
       1 46 110 ; 
       2 31 110 ; 
       5 44 110 ; 
       6 5 110 ; 
       7 5 110 ; 
       8 5 110 ; 
       9 5 110 ; 
       10 5 110 ; 
       11 5 110 ; 
       12 44 110 ; 
       13 12 110 ; 
       14 12 110 ; 
       15 12 110 ; 
       16 12 110 ; 
       17 12 110 ; 
       18 12 110 ; 
       3 69 110 ; 
       4 3 110 ; 
       26 63 110 ; 
       27 53 110 ; 
       28 46 110 ; 
       30 31 110 ; 
       31 64 110 ; 
       32 69 110 ; 
       33 69 110 ; 
       34 39 110 ; 
       35 39 110 ; 
       36 38 110 ; 
       37 38 110 ; 
       39 40 110 ; 
       38 40 110 ; 
       40 31 110 ; 
       41 42 110 ; 
       42 31 110 ; 
       43 31 110 ; 
       46 31 110 ; 
       47 42 110 ; 
       48 63 110 ; 
       49 63 110 ; 
       50 0 110 ; 
       51 53 110 ; 
       52 53 110 ; 
       53 41 110 ; 
       54 41 110 ; 
       55 54 110 ; 
       56 61 110 ; 
       57 54 110 ; 
       58 2 110 ; 
       59 2 110 ; 
       60 61 110 ; 
       61 47 110 ; 
       62 42 110 ; 
       63 47 110 ; 
       64 45 110 ; 
       65 43 110 ; 
       66 43 110 ; 
       67 43 110 ; 
       68 43 110 ; 
       69 0 110 ; 
       19 44 110 ; 
       20 19 110 ; 
       21 19 110 ; 
       22 19 110 ; 
       23 19 110 ; 
       24 19 110 ; 
       25 19 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 1 300 ; 
       5 12 300 ; 
       6 13 300 ; 
       7 14 300 ; 
       8 14 300 ; 
       9 14 300 ; 
       10 14 300 ; 
       11 14 300 ; 
       12 12 300 ; 
       13 14 300 ; 
       14 14 300 ; 
       15 14 300 ; 
       16 14 300 ; 
       17 14 300 ; 
       18 13 300 ; 
       3 10 300 ; 
       4 11 300 ; 
       29 15 300 ; 
       30 15 300 ; 
       31 0 300 ; 
       32 7 300 ; 
       33 7 300 ; 
       34 9 300 ; 
       35 9 300 ; 
       36 9 300 ; 
       37 9 300 ; 
       39 2 300 ; 
       38 2 300 ; 
       40 3 300 ; 
       43 9 300 ; 
       48 4 300 ; 
       49 6 300 ; 
       50 0 300 ; 
       51 5 300 ; 
       52 4 300 ; 
       53 0 300 ; 
       63 0 300 ; 
       65 9 300 ; 
       66 9 300 ; 
       67 9 300 ; 
       68 9 300 ; 
       69 8 300 ; 
       19 12 300 ; 
       20 14 300 ; 
       21 14 300 ; 
       22 14 300 ; 
       23 14 300 ; 
       24 14 300 ; 
       25 13 300 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER TEXTURES2D 
       0 22 400 ; 
       31 14 400 ; 
       31 27 400 ; 
       31 36 400 ; 
       31 6 400 ; 
       31 30 400 ; 
       31 19 400 ; 
       31 9 400 ; 
       31 24 400 ; 
       31 12 400 ; 
       31 13 400 ; 
       31 20 400 ; 
       31 39 400 ; 
       31 0 400 ; 
       31 1 400 ; 
       31 40 400 ; 
       50 23 400 ; 
       50 3 400 ; 
       50 2 400 ; 
       53 15 400 ; 
       53 28 400 ; 
       53 37 400 ; 
       53 7 400 ; 
       53 31 400 ; 
       53 10 400 ; 
       53 18 400 ; 
       53 5 400 ; 
       53 26 400 ; 
       63 16 400 ; 
       63 29 400 ; 
       63 38 400 ; 
       63 8 400 ; 
       63 32 400 ; 
       63 11 400 ; 
       63 17 400 ; 
       63 4 400 ; 
       63 25 400 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER ANIMATION 
       29 0 15000 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS CHAPTER TEXTURES2D 
       7 34 401 ; 
       8 35 401 ; 
       9 33 401 ; 
       9 21 401 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       44 SCHEM 43.76802 15.76462 0 USR SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       0 SCHEM 31.64524 2.558132 0 USR MPRFLG 0 ; 
       1 SCHEM 11.16213 -8.932805 0 USR MPRFLG 0 ; 
       2 SCHEM 64.91203 -6.932806 0 USR MPRFLG 0 ; 
       5 SCHEM 28.76803 13.76462 0 MPRFLG 0 ; 
       6 SCHEM 22.51802 11.76462 0 MPRFLG 0 ; 
       7 SCHEM 25.01802 11.76462 0 MPRFLG 0 ; 
       8 SCHEM 27.51803 11.76462 0 MPRFLG 0 ; 
       9 SCHEM 30.01803 11.76462 0 MPRFLG 0 ; 
       10 SCHEM 32.51804 11.76462 0 MPRFLG 0 ; 
       11 SCHEM 35.01804 11.76462 0 MPRFLG 0 ; 
       45 SCHEM 49.44572 0.3181552 0 USR SRT 1.17012 1.17012 1.17012 0 0 0 0 3.482017 -3.768805 MPRFLG 0 ; 
       12 SCHEM 43.76802 13.76462 0 MPRFLG 0 ; 
       13 SCHEM 50.01802 11.76462 0 MPRFLG 0 ; 
       14 SCHEM 47.51802 11.76462 0 MPRFLG 0 ; 
       15 SCHEM 45.01802 11.76462 0 MPRFLG 0 ; 
       16 SCHEM 42.51802 11.76462 0 MPRFLG 0 ; 
       17 SCHEM 40.01802 11.76462 0 MPRFLG 0 ; 
       18 SCHEM 37.51802 11.76462 0 MPRFLG 0 ; 
       3 SCHEM 34.14524 -1.441868 0 MPRFLG 0 ; 
       4 SCHEM 32.89524 -3.441868 0 MPRFLG 0 ; 
       26 SCHEM 31.16214 -12.93281 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       27 SCHEM 23.66214 -12.93281 0 USR WIRECOL 4 7 MPRFLG 0 ; 
       28 SCHEM 8.662126 -8.932805 0 USR MPRFLG 0 ; 
       29 SCHEM 30.22314 5.488283 0 USR SRT 7.382336 7.382336 7.382336 -1.43 3.519313e-009 2.353932e-007 -8.87112e-006 21.36028 -23.54636 MPRFLG 0 ; 
       30 SCHEM 41.00671 -10.86252 0 USR MPRFLG 0 ; 
       31 SCHEM 42.41211 -4.932808 0 USR MPRFLG 0 ; 
       32 SCHEM 27.89524 -1.441868 0 MPRFLG 0 ; 
       33 SCHEM 30.39524 -1.441868 0 MPRFLG 0 ; 
       34 SCHEM 73.66214 -10.93281 0 USR MPRFLG 0 ; 
       35 SCHEM 76.16214 -10.93281 0 USR MPRFLG 0 ; 
       36 SCHEM 71.16214 -10.93281 0 USR MPRFLG 0 ; 
       37 SCHEM 68.66214 -10.93281 0 USR MPRFLG 0 ; 
       39 SCHEM 74.91214 -8.932805 0 USR MPRFLG 0 ; 
       38 SCHEM 69.91203 -8.932805 0 USR MPRFLG 0 ; 
       40 SCHEM 72.41214 -6.932806 0 USR MPRFLG 0 ; 
       41 SCHEM 18.66212 -8.932805 0 USR MPRFLG 0 ; 
       42 SCHEM 26.16211 -6.932806 0 USR MPRFLG 0 ; 
       43 SCHEM 57.41203 -6.932806 0 USR MPRFLG 0 ; 
       46 SCHEM 9.912126 -6.932806 0 USR MPRFLG 0 ; 
       47 SCHEM 31.16214 -8.932805 0 USR MPRFLG 0 ; 
       48 SCHEM 26.16211 -12.93281 0 USR MPRFLG 0 ; 
       49 SCHEM 28.66211 -12.93281 0 USR MPRFLG 0 ; 
       50 SCHEM 22.89523 0.5581319 0 MPRFLG 0 ; 
       51 SCHEM 21.16212 -12.93281 0 USR MPRFLG 0 ; 
       52 SCHEM 18.66212 -12.93281 0 USR MPRFLG 0 ; 
       53 SCHEM 21.16212 -10.93281 0 USR MPRFLG 0 ; 
       54 SCHEM 14.91211 -10.93281 0 USR MPRFLG 0 ; 
       55 SCHEM 16.16211 -12.93281 0 USR MPRFLG 0 ; 
       56 SCHEM 36.16211 -12.93281 0 USR MPRFLG 0 ; 
       57 SCHEM 13.66211 -12.93281 0 USR MPRFLG 0 ; 
       58 SCHEM 63.66203 -8.932805 0 USR MPRFLG 0 ; 
       59 SCHEM 66.16203 -8.932805 0 USR MPRFLG 0 ; 
       60 SCHEM 33.66211 -12.93281 0 USR MPRFLG 0 ; 
       61 SCHEM 34.91211 -10.93281 0 USR MPRFLG 0 ; 
       62 SCHEM 38.66211 -8.932805 0 USR MPRFLG 0 ; 
       63 SCHEM 28.66211 -10.93281 0 USR MPRFLG 0 ; 
       64 SCHEM 42.41211 -2.932809 0 USR MPRFLG 0 ; 
       65 SCHEM 61.16203 -8.932805 0 USR MPRFLG 0 ; 
       66 SCHEM 58.66203 -8.932805 0 USR MPRFLG 0 ; 
       67 SCHEM 56.16203 -8.932805 0 USR MPRFLG 0 ; 
       68 SCHEM 53.66209 -8.932805 0 USR MPRFLG 0 ; 
       69 SCHEM 32.89524 0.5581319 0 MPRFLG 0 ; 
       19 SCHEM 58.76803 13.76462 0 MPRFLG 0 ; 
       20 SCHEM 65.01802 11.76462 0 MPRFLG 0 ; 
       21 SCHEM 62.51802 11.76462 0 MPRFLG 0 ; 
       22 SCHEM 60.01803 11.76462 0 MPRFLG 0 ; 
       23 SCHEM 57.51803 11.76462 0 MPRFLG 0 ; 
       24 SCHEM 55.01803 11.76462 0 MPRFLG 0 ; 
       25 SCHEM 52.51802 11.76462 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 87.58209 -15.11036 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 91.97749 -16.10857 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 79.51969 -19.14864 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 90.33969 -16.84901 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       4 SCHEM 84.08408 -16.41561 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       5 SCHEM 85.12389 -17.66214 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       6 SCHEM 88.0414 -17.4583 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       7 SCHEM 185.0361 11.42596 0 WIRECOL 1 7 MPRFLG 0 ; 
       8 SCHEM 200.0361 11.42596 0 WIRECOL 1 7 MPRFLG 0 ; 
       9 SCHEM 94.00529 -16.17517 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       10 SCHEM 205.0361 11.42596 0 WIRECOL 1 7 MPRFLG 0 ; 
       12 SCHEM 36.08082 -6.622182 0 USR WIRECOL 1 7 MPRFLG 0 ; 
       11 SCHEM 207.5361 11.42596 0 WIRECOL 1 7 MPRFLG 0 ; 
       13 SCHEM 26.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       14 SCHEM 29 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       15 SCHEM 89.81079 -16.04795 0 USR WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER TEXTURES2D 
       0 SCHEM 121.4839 -32.7776 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       1 SCHEM 123.9839 -32.7776 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       2 SCHEM 83.71668 -35.60802 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       3 SCHEM 83.65579 -37.82373 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       4 SCHEM 88.42098 -19.49441 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       5 SCHEM 90.92098 -19.49441 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       6 SCHEM 89.52979 -36.57135 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       7 SCHEM 72.4464 -36.64484 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       8 SCHEM 77.4464 -36.64484 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       9 SCHEM 92.01229 -35.79421 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       10 SCHEM 74.92899 -35.8677 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       11 SCHEM 79.92899 -35.8677 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       12 SCHEM 113.9839 -34.65081 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       13 SCHEM 113.7593 -35.71515 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       14 SCHEM 94.31899 -35.73804 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       15 SCHEM 77.2356 -35.81154 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       16 SCHEM 82.66378 -40.37331 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       17 SCHEM 82.20258 -33.36562 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       18 SCHEM 84.70258 -33.36562 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       19 SCHEM 94.52979 -36.57135 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       20 SCHEM 116.4839 -32.7776 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       21 SCHEM 93.53379 -18.29684 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       22 SCHEM 90.10809 -38.51265 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       23 SCHEM 86.42509 -40.98904 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       24 SCHEM 95.88789 -32.7776 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       25 SCHEM 105.8879 -32.7776 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       26 SCHEM 108.3879 -32.7776 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       27 SCHEM 84.52979 -36.57135 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       28 SCHEM 67.4464 -36.64484 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       29 SCHEM 72.4464 -36.64484 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       30 SCHEM 92.02979 -36.57135 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       31 SCHEM 74.9464 -36.64484 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       32 SCHEM 79.9464 -36.64484 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       33 SCHEM 95.23219 -17.55801 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       34 SCHEM 187.5361 11.42596 0 WIRECOL 10 7 MPRFLG 0 ; 
       35 SCHEM 202.5361 11.42596 0 WIRECOL 10 7 MPRFLG 0 ; 
       36 SCHEM 87.02979 -36.57135 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       37 SCHEM 69.9464 -36.64484 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       38 SCHEM 74.9464 -36.64484 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       39 SCHEM 125.0805 -28.70372 0 USR WIRECOL 10 7 MPRFLG 0 ; 
       40 SCHEM 119.3479 -28.7417 0 USR WIRECOL 10 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER ANIMATION 
       0 SCHEM 170.0361 14.67692 0 WIRECOL 9 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 80 300 80 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
