SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       MODEL-cam_int1.1-0 ROOT ; 
       MODEL-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 4     
       MODEL-default1.1-0 ; 
       MODEL-default2.1-0 ; 
       MODEL-default3.1-0 ; 
       MODEL-default4.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 21     
       MODEL-bmerge1.1-0 ; 
       MODEL-bmerge2.1-0 ; 
       MODEL-cyl1.1-0 ; 
       MODEL-cyl2.1-0 ; 
       MODEL-extru4.1-0 ; 
       MODEL-extru5.1-0 ; 
       MODEL-extru6.1-0 ; 
       MODEL-extru7.1-0 ; 
       MODEL-extru8.1-0 ; 
       MODEL-extru9.1-0 ; 
       MODEL-face3.1-0 ROOT ; 
       MODEL-null14.1-0 ; 
       MODEL-null15.1-0 ; 
       MODEL-null16.1-0 ; 
       MODEL-PIPE.6-0 ; 
       MODEL-PIPE_1.2-0 ; 
       MODEL-PIPE_2.1-0 ; 
       MODEL-PIPE_3.1-0 ; 
       MODEL-PIPE_4.1-0 ; 
       MODEL-torus3.1-0 ; 
       MODEL-torus5.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       paths-MODEL.1-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       6 11 110 ; 
       18 10 110 ; 
       12 10 110 ; 
       0 11 110 ; 
       11 10 110 ; 
       13 10 110 ; 
       1 13 110 ; 
       7 13 110 ; 
       8 13 110 ; 
       9 13 110 ; 
       2 12 110 ; 
       3 12 110 ; 
       14 10 110 ; 
       15 10 110 ; 
       16 10 110 ; 
       17 10 110 ; 
       19 2 110 ; 
       20 3 110 ; 
       4 11 110 ; 
       5 11 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
       1 2 300 ; 
       1 3 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       6 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       18 SCHEM 25 -2 0 MPRFLG 0 ; 
       12 SCHEM 28.75 -2 0 MPRFLG 0 ; 
       0 SCHEM 21.25 -4 0 MPRFLG 0 ; 
       11 SCHEM 17.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 41.25 -4 0 MPRFLG 0 ; 
       7 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       8 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       9 SCHEM 35 -4 0 MPRFLG 0 ; 
       2 SCHEM 27.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 30 -4 0 MPRFLG 0 ; 
       10 SCHEM 22.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 10 -2 0 MPRFLG 0 ; 
       15 SCHEM 5 -2 0 MPRFLG 0 ; 
       16 SCHEM 2.5 -2 0 MPRFLG 0 ; 
       17 SCHEM 7.5 -2 0 MPRFLG 0 ; 
       19 SCHEM 27.5 -6 0 MPRFLG 0 ; 
       20 SCHEM 30 -6 0 MPRFLG 0 ; 
       4 SCHEM 15 -4 0 MPRFLG 0 ; 
       5 SCHEM 17.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 20 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 22.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       2 SCHEM 40 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       3 SCHEM 42.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
