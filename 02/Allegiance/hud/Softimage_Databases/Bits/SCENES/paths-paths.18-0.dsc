SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       paths-cam_int1.18-0 ROOT ; 
       paths-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 39     
       paths-circle1.1-0 ; 
       paths-circle2.1-0 ; 
       paths-circle3.2-0 ROOT ; 
       paths-cyl1.1-0 ; 
       paths-cyl2.1-0 ; 
       paths-face3.2-0 ROOT ; 
       paths-null10.1-0 ; 
       paths-null11.1-0 ; 
       paths-null12.1-0 ; 
       paths-null13.4-0 ROOT ; 
       paths-null9.1-0 ; 
       paths-nurbs1.1-0 ROOT ; 
       paths-nurbs2.1-0 ROOT ; 
       paths-PIPE.6-0 ; 
       paths-PIPE_1.2-0 ; 
       paths-PIPE_1_1.3-0 ; 
       paths-PIPE_2.1-0 ; 
       paths-PIPE_3.1-0 ; 
       paths-spline2.5-0 ROOT ; 
       paths-spline3.2-0 ROOT ; 
       paths-spline4.2-0 ROOT ; 
       paths-spline5.3-0 ROOT ; 
       paths-spline6.1-0 ROOT ; 
       paths-spl_25.1-0 ; 
       paths-spl_27.1-0 ; 
       paths-spl_28.2-0 ; 
       paths-spl_29.1-0 ; 
       paths-spl_32.1-0 ; 
       paths-spl_33.1-0 ; 
       paths-spl_33_1.1-0 ; 
       paths-spl_33_2.1-0 ; 
       paths-spl_51.1-0 ; 
       paths-spl_52.1-0 ; 
       paths-spl_53.1-0 ; 
       paths-spl_54.1-0 ; 
       paths-spl_55.1-0 ; 
       paths-torus3.1-0 ; 
       paths-torus5.1-0 ; 
       PIPE8-PIPE_1.1-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       paths-paths.18-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       23 7 110 ; 
       1 8 110 ; 
       24 7 110 ; 
       0 8 110 ; 
       25 7 110 ; 
       26 7 110 ; 
       6 9 110 ; 
       30 6 110 ; 
       33 6 110 ; 
       27 7 110 ; 
       28 10 110 ; 
       34 6 110 ; 
       35 6 110 ; 
       13 5 110 ; 
       13 18 220 RELDATA SCLE 0.5017521 2.164793 0.5017521 ROLL 0 TRNS 0 -0.01995975 0 EndOfRELDATA ; 
       13 18 220 2 ; 
       14 5 110 ; 
       14 22 220 RELDATA SCLE 1 0.1743634 1 ROLL 0 TRNS 0 1.445215 0 EndOfRELDATA ; 
       14 22 220 2 ; 
       15 5 110 ; 
       15 21 220 RELDATA SCLE 0.345504 0.515029 0.7319999 ROLL 0 TRNS 0 4.090128 0 EndOfRELDATA ; 
       15 21 220 2 ; 
       3 5 110 ; 
       36 3 110 ; 
       4 5 110 ; 
       37 4 110 ; 
       7 9 110 ; 
       8 9 110 ; 
       16 5 110 ; 
       10 9 110 ; 
       29 10 110 ; 
       31 10 110 ; 
       32 10 110 ; 
       17 5 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       23 SCHEM 35 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       1 SCHEM 7.5 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       24 SCHEM 32.5 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       0 SCHEM 10 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       25 SCHEM 42.5 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       26 SCHEM 40 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       6 SCHEM 26.25 -2 0 DISPLAY 2 0 MPRFLG 0 ; 
       30 SCHEM 30 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       33 SCHEM 22.5 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       27 SCHEM 37.5 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       28 SCHEM 15 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       34 SCHEM 27.5 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       35 SCHEM 25 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       13 SCHEM 60 -2 0 MPRFLG 0 ; 
       18 SCHEM 5 0 0 DISPLAY 0 0 SRT 0.9940001 0.9940001 0.9940001 0 0 0 0 0 0 MPRFLG 0 ; 
       14 SCHEM 55 -2 0 MPRFLG 0 ; 
       15 SCHEM 45 -2 0 MPRFLG 0 ; 
       19 SCHEM 65 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       3 SCHEM 50 -2 0 MPRFLG 0 ; 
       36 SCHEM 50 -4 0 MPRFLG 0 ; 
       4 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       37 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       20 SCHEM 67.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       5 SCHEM 52.5 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       2 SCHEM 70 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       12 SCHEM 75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 72.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       7 SCHEM 37.5 -2 0 DISPLAY 2 0 MPRFLG 0 ; 
       8 SCHEM 8.75 -2 0 DISPLAY 2 0 MPRFLG 0 ; 
       9 SCHEM 25 0 0 DISPLAY 3 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       21 SCHEM 62.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       22 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       38 SCHEM 77.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       10 SCHEM 16.25 -2 0 DISPLAY 2 0 MPRFLG 0 ; 
       29 SCHEM 20 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       31 SCHEM 12.5 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       32 SCHEM 17.5 -4 0 DISPLAY 2 0 MPRFLG 0 ; 
       17 SCHEM 57.5 -2 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
