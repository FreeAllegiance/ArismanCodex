SDSC3.81
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       mat-cam_int1.2-0 ROOT ; 
       mat-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 16     
       static-afuselg.1-0 ; 
       static-ffuselg.1-0 ; 
       static-fig02.2-0 ROOT ; 
       static-finzzz0.1-0 ; 
       static-finzzz1.1-0 ; 
       static-lbooster.1-0 ; 
       static-lfinzzz.1-0 ; 
       static-lwingzz0.1-0 ; 
       static-lwingzz1.1-0 ; 
       static-lwingzz2.1-0 ; 
       static-rbooster.1-0 ; 
       static-rfinzzz.1-0 ; 
       static-rwingzz0.1-0 ; 
       static-rwingzz1.1-0 ; 
       static-rwingzz2.1-0 ; 
       static-thrust0.1-0 ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       ship-mat.2-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       7 2 110 ; 
       8 7 110 ; 
       9 8 110 ; 
       15 2 110 ; 
       5 15 110 ; 
       10 15 110 ; 
       12 2 110 ; 
       13 12 110 ; 
       14 13 110 ; 
       3 2 110 ; 
       6 3 110 ; 
       11 3 110 ; 
       4 3 110 ; 
       1 2 110 ; 
       0 1 110 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       2 SCHEM 8.75 -4 0 SRT 0.1528094 0.1528094 0.1528094 6.264869e-005 -1.569883 0.002353534 0 0 0 MPRFLG 0 ; 
       7 SCHEM 17.5 -6 0 MPRFLG 0 ; 
       8 SCHEM 17.5 -8 0 MPRFLG 0 ; 
       9 SCHEM 17.5 -10 0 MPRFLG 0 ; 
       15 SCHEM 1.25 -6 0 MPRFLG 0 ; 
       5 SCHEM 2.5 -8 0 MPRFLG 0 ; 
       10 SCHEM 0 -8 0 MPRFLG 0 ; 
       12 SCHEM 5 -6 0 MPRFLG 0 ; 
       13 SCHEM 5 -8 0 MPRFLG 0 ; 
       14 SCHEM 5 -10 0 MPRFLG 0 ; 
       3 SCHEM 10 -6 0 MPRFLG 0 ; 
       6 SCHEM 12.5 -8 0 MPRFLG 0 ; 
       11 SCHEM 7.5 -8 0 MPRFLG 0 ; 
       4 SCHEM 10 -8 0 MPRFLG 0 ; 
       1 SCHEM 15 -6 0 MPRFLG 0 ; 
       0 SCHEM 15 -8 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 91 91 1 
       PAUSE 1 
       RATE 15 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
