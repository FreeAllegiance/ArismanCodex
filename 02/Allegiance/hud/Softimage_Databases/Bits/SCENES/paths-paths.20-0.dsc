SDSC3.71
ELEMENTS 
    CHAPTER CAMERAS NBELEM 2     
       paths-cam_int1.20-0 ROOT ; 
       paths-Camera1.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS NBELEM 2     
       paths-default1.1-0 ; 
       paths-default2.1-0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS NBELEM 44     
       paths-bmerge1.1-0 ; 
       paths-circle1.1-0 ; 
       paths-circle2.1-0 ; 
       paths-circle3.3-0 ROOT ; 
       paths-cyl1.1-0 ; 
       paths-cyl2.1-0 ; 
       paths-extru4.1-0 ; 
       paths-extru5.1-0 ; 
       paths-extru6.1-0 ; 
       paths-face3.4-0 ROOT ; 
       paths-face5.1-0 ; 
       paths-null10.1-0 ; 
       paths-null11.1-0 ; 
       paths-null12.1-0 ; 
       paths-null13.5-0 ROOT ; 
       paths-null14.1-0 ; 
       paths-null9.1-0 ; 
       paths-nurbs1.2-0 ROOT ; 
       paths-nurbs2.2-0 ROOT ; 
       paths-PIPE.6-0 ; 
       paths-PIPE_1.2-0 ; 
       paths-PIPE_1_1.3-0 ; 
       paths-PIPE_2.1-0 ; 
       paths-PIPE_3.1-0 ; 
       paths-spline2.6-0 ROOT ; 
       paths-spline3.3-0 ROOT ; 
       paths-spline4.3-0 ROOT ; 
       paths-spline5.4-0 ROOT ; 
       paths-spline6.2-0 ROOT ; 
       paths-spl_25.1-0 ; 
       paths-spl_27.1-0 ; 
       paths-spl_28.2-0 ; 
       paths-spl_29.1-0 ; 
       paths-spl_32.1-0 ; 
       paths-spl_33.1-0 ; 
       paths-spl_33_1.1-0 ; 
       paths-spl_33_2.1-0 ; 
       paths-spl_51.1-0 ; 
       paths-spl_52.1-0 ; 
       paths-spl_53.1-0 ; 
       paths-spl_54.1-0 ; 
       paths-torus3.1-0 ; 
       paths-torus5.1-0 ; 
       PIPE8-PIPE_1.2-0 ROOT ; 
    EndOfCHAPTER 

    CHAPTER SETUP_SOFT NBELEM 1     
       paths-paths.20-0 ; 
    EndOfCHAPTER 

EndOfELEMENTS 

RELATIONS 
    CHAPTER MODELS CHAPTER MODELS 
       8 15 110 ; 
       0 15 110 ; 
       15 9 110 ; 
       1 13 110 ; 
       2 13 110 ; 
       4 9 110 ; 
       5 9 110 ; 
       11 14 110 ; 
       12 14 110 ; 
       13 14 110 ; 
       16 14 110 ; 
       19 9 110 ; 
       20 9 110 ; 
       21 9 110 ; 
       21 27 220 RELDATA SCLE 0.345504 0.515029 0.7319999 ROLL 0 TRNS 0 4.090128 0 EndOfRELDATA ; 
       21 27 220 2 ; 
       22 9 110 ; 
       23 9 110 ; 
       29 12 110 ; 
       30 12 110 ; 
       31 12 110 ; 
       32 12 110 ; 
       33 12 110 ; 
       34 16 110 ; 
       35 16 110 ; 
       36 11 110 ; 
       37 16 110 ; 
       38 16 110 ; 
       39 11 110 ; 
       40 11 110 ; 
       10 11 110 ; 
       41 4 110 ; 
       42 5 110 ; 
       6 15 110 ; 
       7 15 110 ; 
    EndOfCHAPTER 

    CHAPTER MODELS CHAPTER MATERIALS 
       0 0 300 ; 
       0 1 300 ; 
    EndOfCHAPTER 

    CHAPTER CAMERAS CHAPTER CAMERAS 
       1 0 1110 ; 
    EndOfCHAPTER 

EndOfRELATIONS 

ENVIRONMENT 
    CHAPTER CAMERAS 
       0 SCHEM 0 0 0 DISPLAY 0 0 MPRFLG 0 ; 
       1 SCHEM 0 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MODELS 
       8 SCHEM 62.5 -4 0 MPRFLG 0 ; 
       0 SCHEM 71.25 -4 0 MPRFLG 0 ; 
       15 SCHEM 67.5 -2 0 MPRFLG 0 ; 
       1 SCHEM 10 -4 0 MPRFLG 0 ; 
       2 SCHEM 7.5 -4 0 MPRFLG 0 ; 
       3 SCHEM 82.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       4 SCHEM 50 -2 0 MPRFLG 0 ; 
       5 SCHEM 52.5 -2 0 MPRFLG 0 ; 
       9 SCHEM 58.75 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       11 SCHEM 26.25 -2 0 DISPLAY 0 0 MPRFLG 0 ; 
       12 SCHEM 37.5 -2 0 MPRFLG 0 ; 
       13 SCHEM 8.75 -2 0 MPRFLG 0 ; 
       14 SCHEM 25 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       16 SCHEM 16.25 -2 0 MPRFLG 0 ; 
       17 SCHEM 85 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       18 SCHEM 87.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       19 SCHEM 60 -2 0 MPRFLG 0 ; 
       20 SCHEM 55 -2 0 MPRFLG 0 ; 
       21 SCHEM 45 -2 0 MPRFLG 0 ; 
       22 SCHEM 47.5 -2 0 MPRFLG 0 ; 
       23 SCHEM 57.5 -2 0 MPRFLG 0 ; 
       24 SCHEM 5 0 0 DISPLAY 0 0 SRT 0.9940001 0.9940001 0.9940001 0 0 0 0 0 0 MPRFLG 0 ; 
       25 SCHEM 77.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       26 SCHEM 80 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       27 SCHEM 75 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       28 SCHEM 2.5 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       29 SCHEM 35 -4 0 MPRFLG 0 ; 
       30 SCHEM 32.5 -4 0 MPRFLG 0 ; 
       31 SCHEM 42.5 -4 0 MPRFLG 0 ; 
       32 SCHEM 40 -4 0 MPRFLG 0 ; 
       33 SCHEM 37.5 -4 0 MPRFLG 0 ; 
       34 SCHEM 15 -4 0 MPRFLG 0 ; 
       35 SCHEM 20 -4 0 MPRFLG 0 ; 
       36 SCHEM 30 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       37 SCHEM 12.5 -4 0 MPRFLG 0 ; 
       38 SCHEM 17.5 -4 0 MPRFLG 0 ; 
       39 SCHEM 22.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       40 SCHEM 27.5 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       10 SCHEM 25 -4 0 DISPLAY 0 0 MPRFLG 0 ; 
       41 SCHEM 50 -4 0 MPRFLG 0 ; 
       42 SCHEM 52.5 -4 0 MPRFLG 0 ; 
       43 SCHEM 90 0 0 DISPLAY 0 0 SRT 1 1 1 0 0 0 0 0 0 MPRFLG 0 ; 
       6 SCHEM 65 -4 0 MPRFLG 0 ; 
       7 SCHEM 67.5 -4 0 MPRFLG 0 ; 
    EndOfCHAPTER 

    CHAPTER MATERIALS 
       0 SCHEM 70 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
       1 SCHEM 72.5 -6 0 WIRECOL 1 7 MPRFLG 0 ; 
    EndOfCHAPTER 

    PLAYCTRL 
       FRAME 1 100 1 1 
       PAUSE 0 
       RATE 30 
    EndOfPLAYCTRL 
EndOfENVIRONMENT 
